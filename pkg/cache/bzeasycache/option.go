package bzeasycache

import "time"

type Option[T any] func(c *Cache[T])

func WithDefaultExpiresTime[T any](t time.Duration) Option[T] {
	return func(c *Cache[T]) {
		c.defaultExpiresTime = t
	}
}

func WithInitMapCap[T any](cap int) Option[T] {
	return func(c *Cache[T]) {
		c.initMapCap = cap
	}
}
