package bzeasycache

import (
	"sync"
	"time"
)

type Cache[T any] struct {
	m                  map[string]*CacheItem[T] // index 为 pageIndex + pageSize
	lock               sync.RWMutex
	defaultExpiresTime time.Duration
	initMapCap         int
}

type CacheItem[T any] struct {
	Data    T
	EndTime int64
}

func New[T any](opts ...Option[T]) *Cache[T] {
	m := &Cache[T]{}

	if len(opts) > 0 {
		for _, opt := range opts {
			opt(m)
		}
	}

	if m.initMapCap > 0 {
		m.m = make(map[string]*CacheItem[T], m.initMapCap)
	} else {
		m.m = make(map[string]*CacheItem[T])
	}

	return m
}

// Get 获取
func (c *Cache[T]) Get(key string) (T, bool) {
	c.lock.RLock()
	defer c.lock.RUnlock()

	var cache *CacheItem[T]
	cache, ok := c.m[key]
	if ok && cache.EndTime >= time.Now().UnixNano() {
		return cache.Data, true
	}
	return cache.Data, false
}

// Put 设置
func (c *Cache[T]) Put(key string, val T, t ...time.Duration) {
	c.PutLockFunc(key, func() (T, time.Duration, error) {
		if len(t) > 0 {
			return val, t[0], nil
		}
		return val, c.defaultExpiresTime, nil
	})
}

// PutLockFunc 设置并支持幂等性方法
func (c *Cache[T]) PutLockFunc(key string, f func() (T, time.Duration, error)) error {
	c.lock.Lock()
	defer c.lock.Unlock()

	val, t, err := f()
	if err == nil {
		c.m[key] = &CacheItem[T]{Data: val}

		if t > 0 {
			c.m[key].EndTime = time.Now().Add(t).UnixNano()
		} else {
			c.m[key].EndTime = time.Now().Add(c.defaultExpiresTime).UnixNano()
		}
	}
	return err
}

// ExistOrPut 如果不存在则创建，存在则不创建，如redis setnx
// @return bool 是否存在
func (c *Cache[T]) ExistOrPut(key string, val T, t ...time.Duration) bool {
	if _, ok := c.Get(key); !ok {
		c.Put(key, val, t...)
		return false
	}
	return true
}

// GetOrPut 存在则正常获取，不存在则创建默认值并返回
func (c *Cache[T]) GetOrPut(key string, val T, t ...time.Duration) T {
	_ = c.ExistOrPut(key, val, t...)
	return val
}
