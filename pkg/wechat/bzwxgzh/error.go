package bzwxgzh

import "gitee.com/breezeHub/bz/pkg/data/bzjson"

// WxRespErr 微信响应错误信息
// {"errcode":40013,"errmsg":"invalid appid"}
type WxRespErr struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

// ParseWxRespErr 解析微信响应错误信息
// @param b []byte 获取的响应字节
// @return *WxRespErr 微信响应错误信息
// @return error 解析错误信息
func ParseWxRespErr(b []byte) (*WxRespErr, error) {
	return bzjson.Decode[*WxRespErr](b)
}
