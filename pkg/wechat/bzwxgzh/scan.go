package bzwxgzh

import (
	"crypto/sha1"
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"sort"
	"strconv"
	"strings"
	"time"
)

// CheckSignature 验签
// @param checkSignatureToken string 微信平台的服务器验签token
func CheckSignature(checkSignatureToken, timestamp, nonce string) (string, error) {
	// token、timestamp、nonce按字典排序的字符串list
	strs := sort.StringSlice{checkSignatureToken, timestamp, nonce} // 使用本地的token生成校验
	sort.Strings(strs)
	var str strings.Builder
	for _, s := range strs {
		str.WriteString(s)
	}

	// 哈希算法加密list得到hashcode
	h := sha1.New()
	h.Write(bzstring.ToByte(str.String()))
	return fmt.Sprintf("%x", h.Sum(nil)), nil // h.Sum(nil) 做hash
}

const (
	MSGTYPE_SUBSCRIBE   = "subscribe"   // 关注
	MSGTYPE_UNSUBSCRIBE = "unsubscribe" // 取消关注
	MSGTYPE_SCAN        = "SCAN"        // 扫码
	MSGTYPE_TEXT        = "text"        // 用户向公众号发消息
)

/* -------------------------- 接收事件推送 -------------------------- */

// ScanSentMessageXML 扫码后发送模板消息 收到的xml信息
// <xml>
// <ToUserName><![CDATA[gh_2a871094fffc]]></ToUserName>
// <FromUserName><![CDATA[oosZs5iF1s4siyAUBu53vNrFEBBY]]></FromUserName>
// <CreateTime>1684830330</CreateTime>
// <MsgType><![CDATA[event]]></MsgType>
// <Event><![CDATA[SCAN]]></Event>
// <EventKey><![CDATA[{"line_no":11,"table_no":22}]]></EventKey>
// <Ticket><![CDATA[]]></Ticket>
// <Encrypt><![CDATA[]]></Encrypt>
// </xml>
type GetScanEventMsgXml struct {
	ToUserName   string `xml:"ToUserName"`
	FromUserName string `xml:"FromUserName"`
	CreateTime   string `xml:"CreateTime"`
	MsgType      string `xml:"MsgType"`

	Event    string `xml:"Event"`
	EventKey string `xml:"EventKey"`
	Ticket   string `xml:"Ticket"`
	Encrypt  string `xml:"Encrypt"`
}

// GenScanEventMsgXml 接收事件推送：生成发送text消息
// <xml>
// <ToUserName><![CDATA[gh_2a871094fffc]]></ToUserName>
// <FromUserName><![CDATA[oosZs5iF1s4siyAUBu53vNrFEBBY]]></FromUserName>
// <CreateTime>1684830330</CreateTime>
// <MsgType><![CDATA[event]]></MsgType>
// <Content><![CDATA[临时二维码入口进入]]></Content>
// </xml>
func GenScanEventTextMsgXml(memberOpenid, gzhOpenid, content string) string {
	xmlContent := bzstring.ReplaceAllWithBraceByMap(`<xml>
<ToUserName><![CDATA[{{ to }}]]></ToUserName>
<FromUserName><![CDATA[{{ from }}]]></FromUserName>
<CreateTime>{{ time }}</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[{{ content }}]]></Content>
</xml>`, map[string]string{
		"to":      memberOpenid,
		"from":    gzhOpenid,
		"time":    strconv.FormatInt(time.Now().Unix(), 10),
		"content": content,
	})
	return xmlContent
}

/* -------------------------- 被动回复用户消息 -------------------------- */

// <xml>
// <ToUserName><![CDATA[gh_5b013782f4b4]]></ToUserName>
// <FromUserName><![CDATA[oLqAc69qMytBNNVYqj-zQ20pV9pc]]></FromUserName>
// <CreateTime>1690352640</CreateTime>
// <MsgType><![CDATA[text]]></MsgType>
// <Content><![CDATA[2]]></Content>
// <MsgId>24200028314463725</MsgId>
// <Encrypt><![CDATA[]]></Encrypt>
// </xml>
type GetReplyMsgXml struct {
	ToUserName   string `xml:"ToUserName"`
	FromUserName string `xml:"FromUserName"`
	CreateTime   string `xml:"CreateTime"`
	MsgType      string `xml:"MsgType"`

	Content string `xml:"Content"`
	MsgId   int64  `xml:"MsgId"`
	Encrypt string `xml:"Encrypt"`
}

// GenScanSentNewsXmlMessage 被动回复用户消息：生成发送news消息
// <xml>
// <ToUserName><![CDATA[toUser]]></ToUserName>
// <FromUserName><![CDATA[fromUser]]></FromUserName>
// <CreateTime>12345678</CreateTime>
// <MsgType><![CDATA[news]]></MsgType>
// <ArticleCount>1</ArticleCount>
// <Articles>
// <item>
// <Title><![CDATA[title1]]></Title>
// <Description><![CDATA[description1]]></Description>
// <PicUrl><![CDATA[picurl]]></PicUrl>
// <Url><![CDATA[url]]></Url>
// </item>
// </Articles>
// </xml>
func GenReplyNewsMsgXml(memberOpenid, gzhOpenid, title, description, picUrl, url string) string {
	xmlContent := bzstring.ReplaceAllWithBraceByMap(`<xml>
<ToUserName><![CDATA[{{ to }}]]></ToUserName>
<FromUserName><![CDATA[{{ from }}]]></FromUserName>
<CreateTime>{{ time }}</CreateTime>
<MsgType><![CDATA[news]]></MsgType>
<ArticleCount>1</ArticleCount>
<Articles>
<item>
<Title><![CDATA[{{ title }}]]></Title>
<Description><![CDATA[{{ description }}]]></Description>
<PicUrl><![CDATA[{{ picUrl }}]]></PicUrl>
<Url><![CDATA[{{ url }}]]></Url>
</item>
</Articles>
</xml>`, map[string]string{
		"to":          memberOpenid,
		"from":        gzhOpenid,
		"time":        strconv.FormatInt(time.Now().Unix(), 10),
		"title":       title,
		"description": description,
		"picUrl":      picUrl,
		"url":         url,
	})
	return xmlContent
}

// GenReplyTextMsgXml 被动回复用户消息：生成发送text消息
// <xml>
// <ToUserName><![CDATA[toUser]]></ToUserName>
// <FromUserName><![CDATA[fromUser]]></FromUserName>
// <CreateTime>12345678</CreateTime>
// <MsgType><![CDATA[text]]></MsgType>
// <Content><![CDATA[你好]]></Content>
// </xml>
func GenReplyTextMsgXml(memberOpenid, gzhOpenid, content string) string {
	xmlContent := bzstring.ReplaceAllWithBraceByMap(`<xml>
<ToUserName><![CDATA[{{ to }}]]></ToUserName>
<FromUserName><![CDATA[{{ from }}]]></FromUserName>
<CreateTime>{{ time }}</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[{{ content }}]]></Content>
</xml>`, map[string]string{
		"to":      memberOpenid,
		"from":    gzhOpenid,
		"time":    strconv.FormatInt(time.Now().Unix(), 10),
		"content": content,
	})
	return xmlContent
}

// WxScanInfo { "signature":"", "timestamp":"", "nonce":"", "echostr":"" }
type WxScanInfo struct {
	Signature string `form:"signature"`
	Timestamp string `form:"timestamp"`
	Nonce     string `form:"nonce"`
	EchoStr   string `form:"echostr"`
}
