package bzwxgzh

import "gitee.com/breezeHub/bz/pkg/data/bzjson"

type UserInfo struct {
	Openid     string   `json:"openid"`
	Nickname   string   `json:"nickname"`
	Sex        int      `json:"sex"`
	Province   string   `json:"province"`
	City       string   `json:"city"`
	Country    string   `json:"country"`
	HeadImgUrl string   `json:"headimgurl"`
	Privilege  []string `json:"privilege"`
	UnionId    string   `json:"unionid"`
}

// ParseUserInfo 解析微信响应用户信息
// @param b []byte 获取的响应字节
// @return *OpenidInfo 微信响应用户信息
// @return error 解析错误信息
func ParseUserInfo(b []byte) (*UserInfo, error) {
	return bzjson.Decode[*UserInfo](b)
}
