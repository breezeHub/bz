package bzwxgzh

import (
	"fmt"
	"net/url"
)

// CodeUrl 获取微信用户Code
func CodeUrl(appid, redirectUri string) string {
	return fmt.Sprintf(
		"https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo&state=200#wechat_redirect",
		appid, url.QueryEscape(redirectUri))
}

// GetWxUserInfoUrl 获取微信用户信息（昵称、头像...）
func GetWxUserInfoUrl(userAccessToken, openid string) string {
	return fmt.Sprintf("https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN",
		userAccessToken, openid)
}

// AccessTokenUrl 获取access_token
func AccessTokenUrl(appid, secret string) string {
	return fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s",
		appid, secret)
}

// GenQrcodeUrl 生成二维码
func GenQrcodeUrl(accessToken string) string {
	return fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s", accessToken)
}

// GetQrcodeImageUrl 二维码生成的地址
func GetQrcodeImageUrl(ticket string) string {
	return fmt.Sprintf("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s", ticket)
}

// GetOpenidUrl 根据微信code获取openid
func GetOpenidUrl(appid, secret, code string) string {
	return fmt.Sprintf("https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code",
		appid, secret, code)
}
