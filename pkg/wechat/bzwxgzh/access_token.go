package bzwxgzh

import (
	"gitee.com/breezeHub/bz/pkg/code/bzerror"
	"gitee.com/breezeHub/bz/pkg/data/bzjson"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"strconv"
)

// AccessTokenInfo 请求公众号accessToken的响应
// {"access_token":"ACCESS_TOKEN","expires_in":7200}
type AccessTokenInfo struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}

func ParseWxAccessTokenInfo(bs []byte) (*AccessTokenInfo, error) {
	wxRespErr, err := ParseWxRespErr(bs)
	if err != nil {
		return nil, bzerror.Wrap(err, "解析微信错误响应失败")
	}
	if bzstring.IsNotEmpty(wxRespErr.ErrMsg) {
		return nil, bzerror.New("微信错误响应：" + strconv.Itoa(wxRespErr.ErrCode) + "=>" + wxRespErr.ErrMsg)
	}
	return bzjson.Decode[*AccessTokenInfo](bs)
}
