package bzwxgzh

import (
	"gitee.com/breezeHub/bz/pkg/code/bzerror"
	"gitee.com/breezeHub/bz/pkg/data/bzjson"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"gitee.com/breezeHub/bz/pkg/net/bzrequest"
	"strconv"
	"time"
)

type OpenidInfo struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Openid       string `json:"openid"`
	Scope        string `json:"scope"`
}

// ParseOpenidInfo 解析微信响应openid信息
// @param b []byte 获取的响应字节
// @return *OpenidInfo 微信响应openid信息
// @return error 解析错误信息
func ParseOpenidInfo(bs []byte) (*OpenidInfo, error) {
	wxRespErr, err := ParseWxRespErr(bs)
	if err != nil {
		return nil, bzerror.Wrap(err, "解析微信错误响应失败")
	}
	if bzstring.IsNotEmpty(wxRespErr.ErrMsg) {
		return nil, bzerror.New("微信错误响应：" + strconv.Itoa(wxRespErr.ErrCode) + "=>" + wxRespErr.ErrMsg)
	}
	return bzjson.Decode[*OpenidInfo](bs)
}

func GetWxUserInfo(userAccessToken, openid string, timeout time.Duration) (*UserInfo, error) {
	url := GetWxUserInfoUrl(userAccessToken, openid)
	bs, err := bzrequest.Get(url, nil, timeout)
	if err != nil {
		return nil, bzerror.Wrap(err, "获取用户信息响应数据失败")
	}

	// 尝试解析
	info, err := ParseUserInfo(bs)
	if err != nil {
		return nil, bzerror.Wrap(err, "解析用户信息响应数据失败")
	}
	if info.Openid == "" {
		return nil, bzerror.New("获取用户信息为空")
	}
	return info, nil
}
