package bztime

import (
	"context"
	"time"
)

func Ticker(ctx context.Context, task func(ctx context.Context) error, t time.Duration) error {
	if err := task(ctx); err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(t):
			if err := task(ctx); err != nil {
				return err
			}
		}
	}
}

type tickerChannel struct {
	C chan error
}

func TickerNoExit(ctx context.Context, task func(ctx context.Context) error, t time.Duration) *tickerChannel {
	tc := tickerChannel{
		C: make(chan error),
	}

	if err := task(ctx); err != nil {
		tc.C <- err
	}

	go func() {
		for {
			select {
			case <-ctx.Done():
				tc.C <- ctx.Err()
				return
			case <-time.After(t):
				if err := task(ctx); err != nil {
					tc.C <- err
				}
			}
		}
	}()

	return &tc
}

func (c *tickerChannel) Close() {
	close(c.C)
}
