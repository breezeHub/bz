package bzqueue

import "context"

type IQueue interface {
	EnQueue(ctx context.Context, data any) error
	DeQueue(ctx context.Context) (data any, err error)
	IsFull() bool
	IsEmpty() bool
	Len() uint64
}
