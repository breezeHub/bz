package test

import (
	"context"
	"fmt"
	"gitee.com/breezeHub/bz/pkg/os/bzqueue"
	"testing"
	"time"
)

func TestQueue(t *testing.T) {
	q := bzqueue.New[int](10)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	err := q.EnQueue(ctx, 1)
	fmt.Println(err)
}
