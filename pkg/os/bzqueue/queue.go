package bzqueue

import (
	"context"
	"sync"
)

type Queue[T any] struct {
	mtx      *sync.Mutex
	data     []T
	notFull  chan struct{}
	notEmpty chan struct{}
	maxSize  int
}

func New[T any](maxSize int) *Queue[T] {
	mtx := &sync.Mutex{}
	return &Queue[T]{
		data:     make([]T, 0, maxSize),
		mtx:      mtx,
		notFull:  make(chan struct{}, 1),
		notEmpty: make(chan struct{}, 1),
		maxSize:  maxSize,
	}
}

func (q *Queue[T]) EnQueue(ctx context.Context, data T) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	q.mtx.Lock()
	//defer q.mtx.Unlock()

	// 超时控制
	for q.IsFull() {
		// 阻塞，直到被唤醒
		q.mtx.Unlock()

		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-q.notFull:
			q.mtx.Lock()
		}
	}

	q.data = append(q.data, data)
	if len(q.data) == 0 {
		q.notEmpty <- struct{}{}
	}
	q.mtx.Unlock()
	return nil
}

func (q *Queue[T]) DeQueue(ctx context.Context) (data T, err error) {
	var t T

	if ctx.Err() != nil {
		return t, ctx.Err()
	}

	q.mtx.Lock()

	for q.IsEmpty() {
		// 阻塞，等待元素加入
		q.mtx.Unlock()

		select {
		case <-ctx.Done():
			return t, ctx.Err()
		case <-q.notEmpty:
			q.mtx.Lock()
		}
	}

	t = q.data[0]
	q.data = q.data[1:]
	if len(q.data) == q.maxSize-1 {
		q.notFull <- struct{}{}
	}

	//select {
	//case q.notFull <- struct{}{}:
	//default:
	//}
	q.mtx.Unlock()
	return t, nil
}

func (q *Queue[T]) IsFull() bool {
	//TODO implement me
	panic("implement me")
}

func (q *Queue[T]) IsEmpty() bool {
	return len(q.data) == 0
}

func (q *Queue[T]) Len() uint64 {
	return uint64(len(q.data))
}
