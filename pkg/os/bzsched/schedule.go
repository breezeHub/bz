package bzsched

import (
	"context"
	"errors"
	"gitee.com/breezeHub/bz/pkg/os/bzsignal"
	"log"
	"sync"
)

const tag = "[Bz Schedule] "

type schedule struct {
	tasks     map[string]func(context.Context) error
	tasksLock sync.Mutex
	ctx       context.Context
}

func New(ctx context.Context) *schedule {
	return &schedule{
		tasks: make(map[string]func(context.Context) error, 8),
		ctx:   ctx,
	}
}

func (s *schedule) Add(name string, f func(context.Context) error) {
	s.tasksLock.Lock()
	defer s.tasksLock.Unlock()

	s.tasks[name] = f
}

func (s *schedule) Run() error {
	errChan := make(chan error, 1)

	ctx, cancel := context.WithCancel(s.ctx)
	defer cancel()

	//创建退出信号监听
	signal := bzsignal.NewListenExitSignal()

	//监听退出信号
	go func() {
		signal.Wait()

		errChan <- errors.New(tag + "listen exit signal")
		cancel()
	}()

	// 编排开始
	var wg sync.WaitGroup
	for name, task := range s.tasks {
		wg.Add(1)
		name := name
		task := task

		go func() {
			defer wg.Done()

			// 运行任务
			errC := make(chan error, 1)
			go func() {
				if err := task(ctx); err != nil {
					errC <- err
				}
			}()

			// 监听退出
			select {
			case err := <-errC:
				cancel()
				errChan <- errors.New(tag + name + " " + err.Error())
			case <-ctx.Done():
			}
		}()
		log.Println(tag+"Task Run", name)
	}
	wg.Wait()
	return <-errChan
}
