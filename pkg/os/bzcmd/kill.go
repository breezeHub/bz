package bzcmd

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/code/bzerror"
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"gitee.com/breezeHub/bz/pkg/text/bzfile"
	"github.com/pkg/errors"
	"strconv"
)

func PidOf(pidName string) []int {
	var ret []int

	dirs, err := bzfile.DirList("/proc")
	if err != nil {
		return ret
	}

	count := len(dirs)
	for i := 0; i < count; i++ {
		pid, err := strconv.Atoi(dirs[i])
		if err != nil {
			continue
		}

		cmdlineFile := fmt.Sprintf("/proc/%d/cmdline", pid)
		if !bzfile.IsExist(cmdlineFile) {
			continue
		}

		cmdlineBytes, err := bzfile.ReadBytes(cmdlineFile)
		if err != nil {
			continue
		}

		cmdlineBytesLen := len(cmdlineBytes)
		if cmdlineBytesLen == 0 {
			continue
		}

		noNut := make([]byte, 0, cmdlineBytesLen)
		for j := 0; j < cmdlineBytesLen; j++ {
			if cmdlineBytes[j] != 0 {
				noNut = append(noNut, cmdlineBytes[j])
			}
		}

		if bzstring.Contains(bzbyte.ToString(noNut), pidName) {
			ret = append(ret, pid)
		}
	}

	return ret
}

func KillPidOf(pidName string) error {
	if bzstring.IsEmpty(pidName) {
		return errors.New("pid name is empty")
	}

	pids := PidOf(pidName)
	for _, pid := range pids {
		if out, err := KillPid9(pid); err != nil {
			return bzerror.WrapF(err, "kill -9 %d fail, output: %s", pid, out)
		}
	}
	return nil
}

func KillPid9(pid int) (string, error) {
	out, err := CmdOutput("kill", "-9", strconv.Itoa(pid))
	if err != nil {
		return out, errors.Wrapf(err, "kill -9 %d fail, output: %s", pid, out)
	}
	return out, nil
}

func KillPid15(pid int) (string, error) {
	out, err := CmdOutput("kill", "-15", strconv.Itoa(pid))
	if err != nil {
		return out, errors.Wrapf(err, "kill -15 %d fail, output: %s", pid, out)
	}
	return out, nil
}
