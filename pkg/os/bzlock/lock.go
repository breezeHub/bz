package bzlock

import (
	"gitee.com/breezeHub/bz/pkg/data/bzgncmap"
	"sync"
)

var (
	locker   = bzgncmap.New[string, *sync.Mutex]()
	rwLocker = bzgncmap.New[string, *sync.RWMutex]()
)

func Lock(key string) {
	mtx, _ := locker.GetOrPut(key, &sync.Mutex{})
	mtx.Lock()
}

func UnLock(key string) {
	mtx, ok := locker.Get(key)
	if ok {
		mtx.Unlock()
	}
}

func RLock(key string) {
	mtx, _ := rwLocker.GetOrPut(key, &sync.RWMutex{})
	mtx.RLock()
}

func UnRLock(key string) {
	mtx, ok := rwLocker.Get(key)
	if ok {
		mtx.RUnlock()
	}
}

func WLock(key string) {
	mtx, _ := rwLocker.GetOrPut(key, &sync.RWMutex{})
	mtx.Lock()
}

func UnWLock(key string) {
	mtx, ok := rwLocker.Get(key)
	if ok {
		mtx.Unlock()
	}
}

func LockFunc(key string, fn func()) {
	Lock(key)
	defer UnLock(key)

	fn()
}

func RLockFunc(key string, fn func()) {
	RLock(key)
	defer UnRLock(key)

	fn()
}

func WLockFunc(key string, fn func()) {
	WLock(key)
	defer UnWLock(key)

	fn()
}
