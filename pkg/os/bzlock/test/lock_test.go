package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/os/bzlock"
	"testing"
)

func TestLock(t *testing.T) {
	key := "aaa"

	cnt := 0

	go func() {
		//key1 := "bbb"
		for i := 0; i < 5; i++ {
			bzlock.Lock(key)

			fmt.Println(key, i, cnt)
			cnt++
			//time.Sleep(2 * time.Second)
			bzlock.UnLock(key)
		}
	}()

	for i := 0; i < 5; i++ {
		bzlock.Lock(key)

		fmt.Println(key, i, cnt)
		cnt++
		//time.Sleep(2 * time.Second)
		bzlock.UnLock(key)
	}
}
