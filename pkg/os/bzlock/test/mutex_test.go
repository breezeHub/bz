package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/os/bzlock"
	"testing"
)

func TestNewMutex(t *testing.T) {
	m := bzlock.NewMutex()

	// 启动多个goroutine
	for i := 0; i < 5; i++ {
		go func(i int) {
			// 在访问共享资源之前先加锁
			m.Lock()

			fmt.Printf("Goroutine %d 进入临界区\n", i)
			// 假装在处理一些共享资源
			// ...

			fmt.Printf("Goroutine %d 离开临界区\n", i)

			m.UnLock()
		}(i)
	}
}
