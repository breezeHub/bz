package bzlock

type BzRWLocker struct {
	key string
}

func NewRW(key string) *BzRWLocker {
	return &BzRWLocker{key: key}
}

func (l *BzRWLocker) RLock() {
	RLock(l.key)
}

func (l *BzRWLocker) UnRLock() {
	UnRLock(l.key)
}

func (l *BzRWLocker) Lock() {
	Lock(l.key)
}

func (l *BzRWLocker) UnLock() {
	UnLock(l.key)
}

func (l *BzRWLocker) LockFunc(fn func()) {
	LockFunc(l.key, fn)
}

func (l *BzRWLocker) RLockFunc(fn func()) {
	RLockFunc(l.key, fn)
}

func (l *BzRWLocker) WLockFunc(fn func()) {
	WLockFunc(l.key, fn)
}
