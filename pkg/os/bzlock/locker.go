package bzlock

type BzLocker struct {
	key string
}

func New(key string) *BzLocker {
	return &BzLocker{key: key}
}

func (l *BzLocker) Lock() {
	Lock(l.key)
}

func (l *BzLocker) UnLock() {
	UnLock(l.key)
}

func (l *BzLocker) LockFunc(fn func()) {
	LockFunc(l.key, fn)
}
