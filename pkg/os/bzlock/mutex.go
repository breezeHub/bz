package bzlock

import (
	"runtime"
	"sync/atomic"
	"time"
)

type Mutex struct {
	flag int32
}

func NewMutex() *Mutex {
	return &Mutex{}
}

func (m *Mutex) Lock() {
	for !atomic.CompareAndSwapInt32(&m.flag, 0, 1) {
		// 等待自旋锁释放

		waitStartTime := time.Now()
		// 自旋等待锁释放
		for atomic.LoadInt32(&m.flag) != 0 {
			// 添加一些饥饿逻辑，避免线程饥饿
			runtime.Gosched()            // 主动出让时间片
			time.Sleep(time.Millisecond) // 小睡一段时间
			if time.Since(waitStartTime) > time.Millisecond*10 {
				// 如果等待时间超过阈值，强行抢占锁
				runtime.Gosched()
			}
		}
	}
}

func (m *Mutex) UnLock() {
	atomic.StoreInt32(&m.flag, 0)
}
