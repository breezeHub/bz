package bzjson

import (
	"encoding/json"
	"gitee.com/breezeHub/bz/pkg/code/bzerror"
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
)

func IsJson(b []byte) bool {
	return json.Valid(b)
}

// Encode 序列化json成byte
func Encode(v any) ([]byte, error) {
	return json.Marshal(v)
}

// EncodeString 序列化json成string
func EncodeString(v any) (string, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return "", err
	}
	return bzbyte.ToString(b), nil
}

func Decode[T any](data []byte) (T, error) {
	var v T
	err := json.Unmarshal(data, &v)
	return v, err
}

func DecodeAny(data []byte, v any) error {
	return json.Unmarshal(data, v)
}

func DecodeMap[K comparable, V any](data []byte) (map[K]V, error) {
	return Decode[map[K]V](data)
}

func Get[K comparable, V any](data []byte, key K) (V, error) {
	var v V
	m, err := DecodeMap[K, V](data)
	if err != nil {
		return v, err
	}

	v, ok := m[key]
	if !ok {
		return v, bzerror.New("key does not exist")
	}
	return v, nil
}
