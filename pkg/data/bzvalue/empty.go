package bzvalue

import (
	"reflect"
)

func IsEmptyValue(value any) bool {
	// 获取值的反射信息
	v := reflect.ValueOf(value)

	// 判断不同类型的空值
	switch v.Kind() {
	case reflect.String, reflect.Array, reflect.Map, reflect.Slice:
		// 字符串、数组、映射和切片类型的空值判断
		return v.Len() == 0
	case reflect.Bool:
		// 布尔类型的空值判断
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		// 整数类型的空值判断
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		// 无符号整数类型的空值判断
		return v.Uint() == 0
	case reflect.Float32, 32, reflect.Float64:
		// 浮点数类型的空值判断
		return v.Float() == 0
	case reflect.Complex64, reflect.Complex128:
		// 复数类型的空值判断
		return v.Complex() == 0
	case reflect.Interface, reflect.Ptr:
		// 接口类型和指针类型的空值判断
		return v.IsNil()
	default:
		// 其他类型默认为空值
		return v.IsZero()
	}
}
