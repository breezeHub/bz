package bzslice

import "sort"

func StringReverse(s []string) {
	sort.Sort(sort.Reverse(sort.StringSlice(s)))
}

func IntReverse(s []int) {
	sort.Sort(sort.Reverse(sort.IntSlice(s)))
}

func Float64Reverse(s []float64) {
	sort.Sort(sort.Reverse(sort.Float64Slice(s)))
}
