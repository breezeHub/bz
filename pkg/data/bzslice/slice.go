package bzslice

import (
	"reflect"
)

// Contain 判断切片是否包含值（泛型）
// Contain[V any](m []V, value V) bool
// @param m []V 泛型切片
// @param value V 泛型值
// @return bool 是否存在
func Contain[V any](m []V, value V) bool {
	_, ok := Search(m, value)
	return ok
}

// Search 判断切片是否包含值，并返回值对应索引（泛型）
// Search[V any](m []V, value V) (int, bool)
// @param m []V 泛型切片
// @param value V 泛型值
// @return int 值对应的索引，不存在则为-1
// @return bool 是否存在
func Search[V any](m []V, value V) (int, bool) {
	if m == nil || len(m) <= 0 {
		return -1, false
	}

	for index, mValue := range m {
		if reflect.DeepEqual(mValue, value) {
			return index, true
		}
	}
	return -1, false
}

// Prefix 给字符串切片加上前缀
// Prefix(s []string, prefix string) []string
// @param s []string 字符串切片
// @param prefix string 前缀
// @return []string 结果字符串切片
func Prefix(s []string, prefix string) []string {
	return CopyAndForEachWithReturn(s, func(_ int, val string) string {
		return prefix + val
	})
}

// Copy 深拷贝泛型切片
// Copy[T any](s []T) []T
// @param s []T 泛型切片
// @return []T 深拷贝的泛型切片
func Copy[T any](s []T) []T {
	sNew := make([]T, len(s))
	copy(sNew, s)
	return sNew
}

// CopyAndForWithPointer 拷贝并遍历泛型切片（通过指针传递修改值）
// For[T any](s []T, f func(key int, val *T) bool) []T
// @param s []T 泛型切片
// @param f func(key int, val *T) bool func(索引, 值) 是否继续遍历
// @return []T 遍历后的泛型切片
func CopyAndForWithPointer[T any](s []T, f func(key int, val *T) bool) []T {
	sNew := Copy(s)
	for index := range sNew {
		ok := f(index, &sNew[index])
		if !ok {
			break
		}
	}
	return sNew
}

// CopyAndForEachWithReturn 拷贝并遍历泛型切片（通过返回值修改值）
// ForEach[T any](s []T, f func(key int, val T) T) []T
// @param s []T 泛型切片
// @param f func(key int, val T) T func(索引, 值) 修改值
// @return []T 遍历后的泛型切片
func CopyAndForEachWithReturn[T any](s []T, f func(key int, val T) T) []T {
	sNew := make([]T, len(s))
	for index, item := range s {
		sNew[index] = f(index, item)
	}
	return sNew
}

//func MatchMapFunc[V any](s1 []V, s2 []V, fn func(s1Value V, s2Value V) bool) bool {
//	if s1 == nil || s2 == nil || len(s1) <= 0 || len(s2) <= 0 {
//		return false
//	}
//
//	for _, m1Value := range s1 {
//		for _, m2Value := range s2 {
//			if fn(m1Value, m2Value) {
//				return true
//			}
//		}
//	}
//	return false
//}

//func MatchSliceFunc[V any](s1 []V, s2 []V, fn func(m1Value V, m2Value V) bool) bool {
//	if s1 == nil || s2 == nil || len(s1) <= 0 || len(s2) <= 0 {
//		return false
//	}
//
//	for _, m1Value := range s1 {
//		for _, m2Value := range s2 {
//			if fn(m1Value, m2Value) {
//				return true
//			}
//		}
//	}
//	return false
//}
