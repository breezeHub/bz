package bzslice

import "gitee.com/breezeHub/bz/pkg/data/bzstring"

func FilterStringEmpty(s []string) []string {
	sNew := make([]string, 0, len(s))
	for _, item := range s {
		if bzstring.IsEmpty(item) {
			continue
		}
		sNew = append(sNew, item)
	}
	return sNew
}
