package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzslice"
	"testing"
)

func TestSlice(t *testing.T) {
	// has
	{
		s := []string{"breeze", "frame", "is", "very", "good"}
		contain := bzslice.Contain(s, "is")
		fmt.Println(contain)

		contain = bzslice.Contain(s, "best")
		fmt.Println(contain)

		index, has := bzslice.Search(s, "very")
		fmt.Println(index, has)

		index, has = bzslice.Search(s, "best")
		fmt.Println(index, has)
	}

	// for
	{
		s := []string{"tom", "lily", "john"}
		sPrefix := bzslice.Prefix(s, "@")
		fmt.Println(sPrefix)

		sFor := bzslice.CopyAndForWithPointer(s, func(key int, val *string) bool {
			*val = fmt.Sprintf("%d_%s", key, *val)
			return true
		})
		fmt.Println(sFor)

		sForEach := bzslice.CopyAndForEachWithReturn(s, func(key int, val string) string {
			return fmt.Sprintf("%d+%s", key, val)
		})
		fmt.Println(sForEach)
	}
}
