package bztime

import "time"

func GetDateListByTime(t time.Time, layout string, dayNum int, desc bool) []string {
	now := t
	var list []string

	if dayNum <= 0 {
		dayAgo := now.AddDate(0, 0, dayNum)
		if !desc {
			// 正序
			for date := dayAgo.AddDate(0, 0, 1); date.Before(now.AddDate(0, 0, 1)); date = date.AddDate(0, 0, 1) {
				list = append(list, date.Format(layout))
			}
		} else {
			// 倒序
			for date := now; date.After(dayAgo); date = date.AddDate(0, 0, -1) {
				list = append(list, date.Format(layout))
			}
		}
	} else {
		dayAfter := now.AddDate(0, 0, dayNum)
		if !desc {
			// 正序
			for date := now; dayAfter.After(date); date = date.AddDate(0, 0, 1) {
				list = append(list, date.Format(layout))
			}
		} else {
			// 倒序
			for date := dayAfter.AddDate(0, 0, -1); now.Before(date.AddDate(0, 0, 1)); date = date.AddDate(0, 0, -1) {
				list = append(list, date.Format(layout))
			}
		}
	}

	return list
}

func GetMonthListByTime(t time.Time, layout string, monthNum int, desc bool) []string {
	now := t
	var months []string

	if monthNum <= 0 {
		monthAgo := now.AddDate(0, monthNum, 0)
		if !desc {
			// 正序
			for date := monthAgo.AddDate(0, 1, 0); now.After(date.AddDate(0, -1, 0)); date = date.AddDate(0, 1, 0) {
				day := date.Format(layout)
				months = append(months, day)
			}
		} else {
			// 倒序
			for date := now; monthAgo.Before(date); date = date.AddDate(0, -1, 0) {
				day := date.Format(layout)
				months = append(months, day)
			}
		}
	} else {
		monthAfter := now.AddDate(0, monthNum, 0)
		if !desc {
			// 正序
			for date := now; monthAfter.After(date); date = date.AddDate(0, 1, 0) {
				day := date.Format(layout)
				months = append(months, day)
			}
		} else {
			// 倒序
			for date := monthAfter.AddDate(0, -1, 0); now.Before(date.AddDate(0, 1, 0)); date = date.AddDate(0, -1, 0) {
				day := date.Format(layout)
				months = append(months, day)
			}
		}
	}

	return months
}
