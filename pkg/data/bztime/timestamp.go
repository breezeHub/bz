package bztime

import (
	"errors"
	"time"
)

// TimestampFormat timestamp格式化
func TimestampFormat(timestamp int64, layout string) string {
	return time.Unix(timestamp, 0).Format(layout)
}

// TimestampToDatetime timestamp转datetime
func TimestampToDatetime(timestamp int64) string {
	return TimestampFormat(timestamp, DateTime)
}

// ToTimestamp 转timestamp
func ToTimestamp(datetime string, layout string) (int64, error) {
	timestamp, err := time.ParseInLocation(layout, datetime, time.Local)
	if err != nil {
		return 0, errors.New("请输入正确格式的时间")
	}
	return timestamp.Unix(), nil
}

// DateTimeToTimestamp datetime转timestamp
func DateTimeToTimestamp(datetime string) (int64, error) {
	return ToTimestamp(datetime, DateTime)
}

func IsTodayByTimestamp(timestamp int64) bool {
	now := time.Now()
	t := time.Unix(timestamp, 0).In(now.Location())
	return t.Year() == now.Year() && t.Month() == now.Month() && t.Day() == now.Day()
}
