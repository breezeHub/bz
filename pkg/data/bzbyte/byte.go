package bzbyte

import (
	"unsafe"
)

// ToString 将byte转为string
// @param b []byte 需要转成string的byte
// @return string 转换结果
func ToString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
