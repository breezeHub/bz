package bzgncmap

// putFunc 写锁操作
func (m *Map[K, V]) putFunc(fn func(map[K]V)) {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	fn(m.m)

	if m.lazyShrink {
		m.shrink()
	}
}

// getFunc 读锁操作
func (m *Map[K, V]) getFunc(fn func(map[K]V)) {
	if m.lazyShrink {
		m.shrink()
	}

	m.mtx.RLock()
	defer m.mtx.RUnlock()

	fn(m.m)
}
