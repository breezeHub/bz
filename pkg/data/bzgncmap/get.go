package bzgncmap

// Get 获取
// @param key K 键
// @return val V 值
// @return exist bool 是否存在
func (m *Map[K, V]) Get(key K) (V, bool) {
	var v V
	var ok bool
	m.getFunc(func(ms map[K]V) {
		v, ok = m.m[key]
	})

	return v, ok
}

// GetOrDefault 获取或得到默认值
// @param key K 键
// @return val V 值
// @return exist bool 是否存在
func (m *Map[K, V]) GetOrDefault(key K, def V) (V, bool) {
	v, ok := m.Get(key)
	if !ok {
		return def, ok
	}
	return v, ok
}

// GetOrPut 获取或新增
// @param key K 键
// @return val V 值
// @return exist bool 是否存在
func (m *Map[K, V]) GetOrPut(key K, val V) (V, bool) {
	v, ok := m.Get(key)
	if !ok {
		m.Put(key, val)
		return val, ok
	}
	return v, ok
}

func (m *Map[K, V]) ExistPut(key K, f func(val *V)) bool {
	if v, ok := m.Get(key); ok {
		m.putFunc(func(ms map[K]V) {
			f(&v)
			ms[key] = v
		})

		return true
	}
	return false
}

// Exist 是否存在
// @param key K 键
// @return exist bool 是否存在
func (m *Map[K, V]) Exist(key K) bool {
	_, ok := m.Get(key)
	return ok
}
