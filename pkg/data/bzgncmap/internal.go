package bzgncmap

import "sync/atomic"

// 缩容
func (m *Map[K, V]) shrink() {
	length := int32(len(m.m))

	// 计算是否需要缩容
	if capacity, ok := calcCapacity(m.count, length); ok {
		m.putFunc(func(ms map[K]V) { m.newMap(capacity) })
	}
}

// 计算是否需要缩容
func calcCapacity(c, l int32) (int32, bool) {
	if c <= 64 {
		return c, false
	}

	if c > 2048 && (c/l >= 2) {
		factor := 0.625
		return int32(float32(c) * float32(factor)), true
	}

	if c <= 2048 && (c/l >= 4) {
		return c / 2, true
	}
	return c, false
}

// 创建新map
func (m *Map[K, V]) newMap(length int32) {
	newM := make(map[K]V, length)
	for k, v := range m.m {
		newM[k] = v
	}
	m.m = newM
	atomic.StoreInt32(&m.count, length)
}
