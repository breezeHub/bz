package bzgncmap

// Delete 删除
// @param key K 键
func (m *Map[K, V]) Delete(keys ...K) {
	m.putFunc(func(ms map[K]V) {
		for _, key := range keys {
			delete(m.m, key)
		}
	})
}

// Flush 清空
func (m *Map[K, V]) Flush() {
	m.putFunc(func(ms map[K]V) {
		m.m = make(map[K]V, m.initCap)
	})
}
