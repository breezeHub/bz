package bzgncmap

type MapOption[K comparable, V comparable] func(m *Map[K, V])

func LazyShrink[K comparable, V comparable](b bool) MapOption[K, V] {
	return func(m *Map[K, V]) {
		m.lazyShrink = b
	}
}

func InitCap[K comparable, V comparable](cap int) MapOption[K, V] {
	return func(m *Map[K, V]) {
		m.initCap = cap
	}
}

func IsUnique[K comparable, V comparable](isUnique bool) MapOption[K, V] {
	return func(m *Map[K, V]) {
		m.isUnique = isUnique
	}
}
