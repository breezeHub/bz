package bzgncmap

// Search 自定义查询
// @param key K 键
// @return val V 值
// @return exist bool 是否存在
func (m *Map[K, V]) Search(fn func(key K, value V) bool) map[K]V {
	newM := make(map[K]V)
	m.getFunc(func(ms map[K]V) {
		for key, val := range ms {
			if fn(key, val) {
				newM[key] = val
			}
		}
	})
	return newM
}
