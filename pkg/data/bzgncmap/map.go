package bzgncmap

import (
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
	jsoniter "github.com/json-iterator/go"
	"reflect"
	"sync"
	"sync/atomic"
	"time"
)

var json = jsoniter.ConfigFastest

type Map[K comparable, V comparable] struct {
	m     map[K]V
	mtx   sync.RWMutex
	count int32 // 累计 cap

	lazyShrink bool // 是否懒缩容，暂未支持良好
	initCap    int  // 预设容量
	isUnique   bool // 是否限制值 必须唯一
}

func New[K comparable, V comparable](opts ...MapOption[K, V]) *Map[K, V] {
	m := &Map[K, V]{}

	for _, opt := range opts {
		opt(m)
	}

	m.m = make(map[K]V, m.initCap)
	if !m.lazyShrink {
		go func() {
			for {
				m.shrink()
				time.Sleep(50 * time.Millisecond)
			}
		}()
	}

	return m
}

// Cap 获取map容量
func (m *Map[K, V]) Cap() int32 {
	return m.count
}

// Len 获取map数量
func (m *Map[K, V]) Len() int32 {
	return int32(len(m.m))
}

// IsEmpty 获取map是否为空
func (m *Map[K, V]) IsEmpty() bool {
	return len(m.m) == 0
}

// GetKeys 获取所有key
func (m *Map[K, V]) GetKeys() []K {
	var ks []K
	m.getFunc(func(ms map[K]V) {
		for k := range ms {
			ks = append(ks, k)
		}
	})
	return ks
}

// GetValues 获取所有value
func (m *Map[K, V]) GetValues() []V {
	var vs []V
	m.getFunc(func(ms map[K]V) {
		for _, v := range ms {
			vs = append(vs, v)
		}
	})
	return vs
}

// Copy 复制
func (m *Map[K, V]) Copy() map[K]V {
	newM := make(map[K]V, len(m.m))
	m.getFunc(func(ms map[K]V) {
		for k, v := range ms {
			newM[k] = v
		}
	})
	return newM
}

// Result 获取结果
func (m *Map[K, V]) Result() map[K]V {
	return m.m
}

// Json 获取json结果
func (m *Map[K, V]) Json() (string, error) {
	bytes, err := json.Marshal(m.m)
	return bzbyte.ToString(bytes), err
}

// IsEqual 判断map是否相同
func (m *Map[K, V]) IsEqual(newM *Map[K, V]) bool {
	var ok bool
	m.getFunc(func(ms map[K]V) {
		ok = reflect.DeepEqual(m.m, newM.m)
	})
	return ok
}

// ValueCount 获取value出现次数
func (m *Map[K, V]) ValueCount(value V) int32 {
	var count int32
	m.getFunc(func(ms map[K]V) {
		for _, v := range ms {
			v := v
			go func() {
				if v == value {
					atomic.AddInt32(&count, 1)
				}
			}()
		}
	})
	return count
}
