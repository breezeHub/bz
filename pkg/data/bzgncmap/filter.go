package bzgncmap

import (
	"gitee.com/breezeHub/bz/pkg/data/bzvalue"
	"reflect"
)

// FilterEmpty 部分空值过滤
func (m *Map[K, V]) FilterEmpty() {
	m.putFunc(func(ms map[K]V) {
		for key, val := range ms {
			switch reflect.ValueOf(val).Kind() {
			case reflect.String, reflect.Array, reflect.Map, reflect.Slice, reflect.Interface, reflect.Ptr:
				if bzvalue.IsEmptyValue(val) {
					delete(m.m, key)
				}
			default:
			}
		}
	})
}

// FilterNil 空值过滤
func (m *Map[K, V]) FilterNil() {
	m.putFunc(func(ms map[K]V) {
		for key, val := range ms {
			if bzvalue.IsEmptyValue(val) {
				delete(m.m, key)
			}
		}
	})
}

// Turn k v 翻转
func (m *Map[K, V]) Turn() map[V]K {
	newM := make(map[V]K, len(m.m))
	m.getFunc(func(ms map[K]V) {
		for k, v := range m.m {
			newM[v] = k
		}
	})
	return newM
}

// FilterUniqueValue 重复值过滤
func (m *Map[K, V]) FilterUniqueValue() {
	cache := make(map[V]struct{}, len(m.m))
	m.putFunc(func(ms map[K]V) {
		for key, val := range ms {
			if _, ok := cache[val]; !ok {
				cache[val] = struct{}{}
			} else {
				delete(m.m, key)
			}
		}
	})
}
