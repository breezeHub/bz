package bzgncmap

import (
	"errors"
	"gitee.com/breezeHub/bz/pkg/data/bzmap"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"sync/atomic"
)

// Put 新增
// @param key K 键
// @param val V 值
func (m *Map[K, V]) Put(key K, val V) bool {
	allow := true
	if m.isUnique {
		m.getFunc(func(ms map[K]V) {
			for k, v := range ms {
				if k != key && v == val {
					allow = false
					return
				}
			}
		})
	}
	if !allow {
		return false
	}

	m.putFunc(func(ms map[K]V) {
		atomic.AddInt32(&m.count, 1)
		m.m[key] = val
	})
	return true
}

// PutMany 新增多个
func (m *Map[K, V]) PutMany(ms map[K]V) bool {
	if len(ms) <= 0 {
		return false
	}

	allow := true
	if m.isUnique {
		m.getFunc(func(_ map[K]V) {
			allow = !bzmap.MatchMapFunc[K, V](m.m, ms, func(key K, value V, targetKey K, targetValue V) bool {
				return key != targetKey && value == targetValue
			})
		})
	}
	if !allow {
		return false
	}

	m.putFunc(func(_ map[K]V) {
		atomic.AddInt32(&m.count, int32(len(ms)))
		for key, val := range ms {
			m.m[key] = val
		}
	})
	return true
}

// Merge 合并
func (m *Map[K, V]) Merge(newM *Map[K, V]) bool {
	if len(newM.m) <= 0 {
		return false
	}

	allow := true
	if m.isUnique {
		m.getFunc(func(_ map[K]V) {
			allow = !bzmap.MatchMapFunc[K, V](m.m, newM.m, func(key K, value V, targetKey K, targetValue V) bool {
				return key != targetKey && value == targetValue
			})
		})
	}
	if !allow {
		return false
	}

	m.putFunc(func(_ map[K]V) {
		atomic.AddInt32(&m.count, int32(len(newM.m)))
		for key, val := range newM.m {
			m.m[key] = val
		}
	})

	return true
}

// AddJson 合并json
func (m *Map[K, V]) AddJson(jsonString string) error {
	if len(jsonString) <= 0 {
		return errors.New("json is null")
	}

	var newM map[K]V
	if err := json.Unmarshal(bzstring.ToByte(jsonString), &newM); err != nil {
		return err
	}

	allow := true
	if m.isUnique {
		m.getFunc(func(_ map[K]V) {
			allow = !bzmap.MatchMapFunc[K, V](m.m, newM, func(key K, value V, targetKey K, targetValue V) bool {
				return key != targetKey && value == targetValue
			})
		})
	}
	if !allow {
		return errors.New("has same value")
	}

	m.putFunc(func(_ map[K]V) {
		atomic.AddInt32(&m.count, int32(len(newM)))
		for key, val := range newM {
			m.m[key] = val
		}
	})
	return nil
}
