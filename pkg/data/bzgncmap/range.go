package bzgncmap

// Range 遍历
func (m *Map[K, V]) Range(fn func(K, V) bool) {
	m.getFunc(func(ms map[K]V) {
		for k, v := range m.m {
			if !fn(k, v) {
				return
			}
		}
	})
}
