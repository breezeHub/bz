package bzgncmap

import "sync/atomic"

// GetRand 随机获取 map
func (m *Map[K, V]) GetRand(count int) map[K]V {
	if count <= 0 {
		return nil
	}

	if int(m.Len()) < count {
		count = int(m.Len())
	}

	// 根据 key 匹配 value
	newM := make(map[K]V, count)
	var i int32
	m.getFunc(func(ms map[K]V) {
		for key, value := range ms {
			it := atomic.LoadInt32(&i)
			if it > int32(count-1) {
				break
			}

			atomic.AddInt32(&i, 1)
			newM[key] = value
		}
	})
	return newM
}
