package bzint

import "fmt"

// ZeroFill 补零
func ZeroFill(i int, len int) string {
	return fmt.Sprintf("%0*d", len, i)
}

// UnZeroFill 取消补零
func UnZeroFill(iString string) (int, error) {
	var num int
	_, err := fmt.Sscanf(iString, "%d", &num)
	return num, err
}
