package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzint"
	"testing"
)

func TestZeroFill(t *testing.T) {
	s := bzint.ZeroFill(1, 5)
	fmt.Println(s)

	i, err := bzint.UnZeroFill(s)
	fmt.Println(i, err)
}
