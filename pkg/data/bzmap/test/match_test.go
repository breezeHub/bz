package test

import (
	"gitee.com/breezeHub/bz/pkg/code/bzdump"
	"gitee.com/breezeHub/bz/pkg/data/bzmap"
	"testing"
)

func TestContain(t *testing.T) {
	m := map[string]int{
		"a": 1,
		"b": 2,
	}
	contain := bzmap.Contain(m, 2)
	bzdump.D(contain)
}
