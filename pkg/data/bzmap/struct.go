package bzmap

import "gitee.com/breezeHub/bz/pkg/data/bzjson"

func ToMap[K comparable, V any](v any) (map[K]V, error) {
	bs, err := bzjson.Encode(v)
	if err != nil {
		return nil, err
	}

	return bzjson.DecodeMap[K, V](bs)
}
