package bzmap

import "reflect"

// Contain 判断泛型map是否包含某值
// func Contain[K comparable, V any](m map[K]V, value V) bool
// @param m map[K]V 查找的map
// @param value V 包含的目标值
// @return bool 是否包含
func Contain[K comparable, V any](m map[K]V, value V) bool {
	if m == nil || len(m) <= 0 {
		return false
	}

	for _, mValue := range m {
		if reflect.DeepEqual(mValue, value) {
			return true
		}
	}
	return false
}

// MatchMapFunc 判断两个map是否符合自定义匹配
// MatchMapFunc[K comparable, V any](m1 map[K]V, m2 map[K]V, fn func(m1Key K, m1Value V, m2Key K, m2Value V) bool) bool
// @param m1 map[K]V 匹配的map 1
// @param m2 map[K]V 匹配的map 2
// @param fn func(m1Key K, m1Value V, m2Key K, m2Value V) bool 自定义匹配函数
// @return bool 是否匹配
func MatchMapFunc[K comparable, V any](m1 map[K]V, m2 map[K]V, fn func(m1Key K, m1Value V, m2Key K, m2Value V) bool) bool {
	if m1 == nil || m2 == nil || len(m1) <= 0 || len(m2) <= 0 {
		return false
	}

	for m1Key, m1Value := range m1 {
		for m2Key, m2Value := range m2 {
			if fn(m1Key, m1Value, m2Key, m2Value) {
				return true
			}
		}
	}
	return false
}

// MatchSliceFunc 判断map和slice是否符合自定义匹配
// MatchSliceFunc[K comparable, V any](m1 map[K]V, m2 []V, fn func(m1Key K, m1Value V, m2Value V) bool) bool
// @param m1 map[K]V 匹配的map
// @param m2 map[K]V 匹配的slice
// @param fn func(m1Key K, m1Value V, m2Value V) bool 自定义匹配函数
// @return bool 是否匹配
func MatchSliceFunc[K comparable, V any](m1 map[K]V, m2 []V, fn func(m1Key K, m1Value V, m2Value V) bool) bool {
	if m1 == nil || m2 == nil || len(m1) <= 0 || len(m2) <= 0 {
		return false
	}

	for m1Key, m1Value := range m1 {
		for _, m2Value := range m2 {
			if fn(m1Key, m1Value, m2Value) {
				return true
			}
		}
	}
	return false
}
