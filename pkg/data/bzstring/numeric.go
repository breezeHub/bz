package bzstring

// IsNumeric 判断字符串是否数字
// IsNumeric(s string) bool
// @param s string 字符串
// @return bool 是否数字
func IsNumeric(s string) bool {
	if _, err := ToInt(s); err != nil {
		_, err = ToFloat(s)
		return err == nil
	}
	return true
}
