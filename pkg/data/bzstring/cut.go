package bzstring

import (
	"strings"
)

func Cut(s, start, end string) string {
	if IsEmpty(s) {
		return s
	}

	if IsNotEmpty(start) {
		startIndex := strings.Index(s, start)
		if startIndex != -1 {
			s = s[startIndex+len(start):]
		}
	}

	if IsNotEmpty(end) {
		endIndex := strings.Index(s, end)
		if endIndex != -1 {
			s = s[:endIndex]
		}
	}

	return s
}

func Substr(s string, start int, length ...int) string {
	if IsEmpty(s) || start < 0 {
		return s
	}

	if len(s) < start {
		return ""
	}

	if len(length) > 0 && start+length[0] < len(s) {
		return s[start : start+length[0]]
	}
	return s[start:]
}
