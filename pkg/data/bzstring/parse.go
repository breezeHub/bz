package bzstring

import (
	"fmt"
	"reflect"
	"strconv"
	"unsafe"
)

// ToInt 字符串转int
// ToInt(s string) (int64, error)
// @param s string 字符串
// @return int64 转换的int结果
// @return error 错误信息
func ToInt(s string) (int64, error) {
	return strconv.ParseInt(s, 10, 64)
}

// ToFloat 字符串转float
// ToFloat(s string) (float64, error)
// @param s string 字符串
// @return float64 转换的float结果
// @return error 错误信息
func ToFloat(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}

func ToDecimal(num float64, i int) string {
	format := fmt.Sprintf("%%.%df", i)
	return fmt.Sprintf(format, num)
}

// ToByte 字符串转[]byte
// ToByte(s string) []byte
// @param s string 字符串
// @return []byte 转换的[]byte结果
func ToByte(s string) []byte {
	var b []byte

	/* #nosec G103 */
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))

	/* #nosec G103 */
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))

	bh.Data, bh.Cap, bh.Len = sh.Data, sh.Len, sh.Len
	return b
}
