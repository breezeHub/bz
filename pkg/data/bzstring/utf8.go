package bzstring

import "github.com/axgle/mahonia"

func Utf8Encode(s string) string {
	utf8Encoder := mahonia.NewEncoder("UTF-8")
	return utf8Encoder.ConvertString(s)
}
