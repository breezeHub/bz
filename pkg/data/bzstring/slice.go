package bzstring

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bztype"
	"strings"
)

// Split 切分字符串
// Split(s string, sep string) []string
// @param s string 字符串
// @param sep string 分割字符
// @return []string 切分后的字符串切片
func Split(s string, sep string) []string {
	return strings.Split(s, sep)
}

// SplitN 切分字符串为N份
// SplitN(s string, sep string) []string
// @param s string 字符串
// @param sep string 分割字符
// @param n int 切片结果的数量
// @return []string 切分后的字符串切片
func SplitN(s string, sep string, n int) []string {
	return strings.SplitN(s, sep, n)
}

// SplitRetain 切分字符串并保留分割字符
// SplitRetain(s string, sep string) []string
// @param s string 字符串
// @param sep string 分割字符
// @return []string 切分后的字符串切片
func SplitRetain(s string, sep string) []string {
	return strings.SplitAfter(s, sep)
}

// SplitRetainN 切分字符串为N份并保留分割字符
// SplitRetainN(s string, sep string) []string
// @param s string 字符串
// @param sep string 分割字符
// @param n int 切片结果的数量
// @return []string 切分后的字符串切片
func SplitRetainN(s string, sep string, n int) []string {
	return strings.SplitAfterN(s, sep, n)
}

// Join	将切片合并为字符串
// Join(s []string, sep string) string
// @param s []string 字符串切片
// @param sep string 拼接字符
// @return string 合并后的字符串
func Join(s []string, sep string) string {
	return strings.Join(s, sep)
}

// JoinVal	将切片合并为字符串
// JoinVal[T bztype.Numeric | bool](s []T, sep string) string
// @param s []string 数值或布尔值切片
// @param sep string 拼接字符
// @return string 合并后的字符串
func JoinVal[T bztype.Numeric | bool](s []T, sep string) string {
	sNew := make([]string, 0, len(s))
	for _, item := range s {
		sNew = append(sNew, fmt.Sprintf("%v", item))
	}
	return Join(sNew, sep)
}

type joinAnyStruct struct {
	val string
}

var (
	JoinAnyStructKV  = joinAnyStruct{val: "%+v"}
	JoinAnyStructAll = joinAnyStruct{val: "%#v"}
)

// JoinAny	将切片合并为字符串
// JoinAny[T any](s []T, sep string, typ joinAnyType) string
// @param s []string 字符串切片
// @param sep string 拼接字符
// @param typs ...joinAnyStruct 结构体格式化可选类型：默认仅输出值、JoinAnyStructKV输出字段和值、JoinAnyStructAll输出完整结构体
// @return string 合并后的字符串
func JoinAny[T any](s []T, sep string, typs ...joinAnyStruct) string {
	typ := "%v"
	if len(typs) > 0 {
		typ = typs[0].val
	}

	sNew := make([]string, 0, len(s))
	for _, item := range s {
		sNew = append(sNew, fmt.Sprintf(typ, item))
	}
	return Join(sNew, sep)
}
