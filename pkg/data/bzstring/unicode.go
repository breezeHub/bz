package bzstring

// RuneLen 计算unicode字符串长度
// RuneLen(s string) int
// @param s string 字符串
// @return int 长度
func RuneLen(s string) int {
	r := []rune(s)
	return len(r)
}
