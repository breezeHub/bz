package bzstring

import "strings"

func Index(s, substr string) int {
	return strings.Index(s, substr)
}

func IndexIgnore(s, substr string) int {
	s = ToLower(s)
	return Index(s, substr)
}

func IndexRune(s string, r rune) int {
	return strings.IndexRune(s, r)
}

func IndexRuneAny(s string, r rune) int {
	s = ToLower(s)
	return IndexRune(s, r)
}
