package bzstring

import (
	"strings"
)

// FilterName 过滤姓名
// FilterName(name string) string
// @param name string 需要过滤的姓名
// @return string 过滤后的姓名
func FilterName(name string) string {
	return FilterWithMarkFirstAndLast(name, "*")
}

// FilterIDNumber 过滤身份证号码
func FilterIDNumber(idNumber string) string {
	return FilterWithMarkRetainLastN(idNumber, "*", 4)
}

func FilterWithMarkRetainFirstN(s, tag string, n int) string {
	return FilterWithMarkRetainN(s, tag, n, 0)
}

func FilterWithMarkRetainLastN(s, tag string, n int) string {
	return FilterWithMarkRetainN(s, tag, 0, n)
}

func FilterWithMarkRetainN(s, tag string, firstN, lastN int) string {
	r := []rune(s)
	length := len(r)

	if length <= 0 || (firstN == 0 && lastN == 0) {
		return s
	}

	if length <= firstN+lastN {
		return Repeat(tag, length)
	}

	if firstN <= 0 {
		return Repeat(tag, length-lastN) + string(r[length-lastN:])
	} else if lastN <= 0 {
		return string(r[:firstN]) + Repeat(tag, length-firstN)
	}

	return string(r[:firstN]) + strings.Repeat(tag, length-firstN-lastN) + string(r[length-firstN-lastN:])
}

func FilterWithMarkFirstAndLast(s, tag string) string {
	// 一个字符
	chars := []rune(s)
	length := len(chars)
	if length <= 1 {
		return tag
	}

	// 两个字符
	if length == 2 {
		return string(chars[0]) + tag
	}

	// 多个字符保留第一个字符和最后一个字符，中间的字用 "*" 替代
	firstChar := string(chars[0])
	lastChar := string(chars[length-1])

	// 将姓氏和名字拼接起来返回
	return firstChar + strings.Repeat(tag, length-2) + lastChar
}
