package bzstring

import "fmt"

func AsciiChar(i int) string {
	return fmt.Sprintf("%c", i)
}

func AsciiNum(s string) (int, error) {
	var asciiCode int
	_, err := fmt.Sscanf(s, "%c", &asciiCode)
	return asciiCode, err
}
