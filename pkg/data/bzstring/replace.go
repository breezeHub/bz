package bzstring

import "strings"

func Replace(s, old, new string, n ...int) string {
	if len(n) > 0 {
		return strings.Replace(s, old, new, n[0])
	}
	return strings.ReplaceAll(s, old, new)
}

func ReplaceAllByMap(s string, m map[string]string) string {
	if m == nil || len(m) <= 0 {
		return s
	}

	for tag, replace := range m {
		s = Replace(s, tag, replace)
	}
	return s
}

// ReplaceAllWithBraceByMap 花括号解析
func ReplaceAllWithBraceByMap(s string, m map[string]string) string {
	if m == nil || len(m) <= 0 {
		return s
	}

	for tag, replace := range m {
		tag = "{{ " + tag + " }}"
		s = strings.ReplaceAll(s, tag, replace)
	}
	return s
}

// ReplaceAllWithSquareBraceByMap 方括号解析
func ReplaceAllWithSquareBraceByMap(s string, m map[string]string) string {
	if m == nil || len(m) <= 0 {
		return s
	}

	for tag, replace := range m {
		tag = "[[ " + tag + " ]]"
		s = strings.ReplaceAll(s, tag, replace)
	}
	return s
}
