package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestSlice(t *testing.T) {
	// split
	{
		splitString := "a|b|c|d"

		split := bzstring.Split(splitString, "|")
		fmt.Println(split)

		split = bzstring.SplitN(splitString, "|", 2)
		fmt.Println(split)

		split = bzstring.SplitRetain(splitString, "|")
		fmt.Println(split)

		split = bzstring.SplitRetainN(splitString, "|", 2)
		fmt.Println(split)
	}

	// join
	{
		ss := []string{"breeze", "frame", "good"}
		join := bzstring.Join(ss, "-")
		fmt.Println(join)

		sa := []int{99, 73, 85, 66}
		joinVal := bzstring.JoinVal(sa, "@")
		fmt.Println(joinVal)

		type test struct {
			Name string
		}
		sst := []test{
			{Name: "xiaoming"},
			{Name: "xiaohong"},
		}
		joinAny := bzstring.JoinAny(sst, "*")
		fmt.Println(joinAny)
		joinAny = bzstring.JoinAny(sst, "*", bzstring.JoinAnyStructKV)
		fmt.Println(joinAny)
		joinAny = bzstring.JoinAny(sst, "*", bzstring.JoinAnyStructAll)
		fmt.Println(joinAny)
	}
}
