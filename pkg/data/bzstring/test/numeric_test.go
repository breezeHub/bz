package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestIsNumeric(t *testing.T) {
	ok := bzstring.IsNumeric("88")
	fmt.Println(ok)

	ok = bzstring.IsNumeric("3.1415926")
	fmt.Println(ok)

	ok = bzstring.IsNumeric("abc")
	fmt.Println(ok)
}
