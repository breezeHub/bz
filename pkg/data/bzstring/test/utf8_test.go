package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestUtf8Encode(t *testing.T) {
	s := "{\"code\":200,\"message\":\"success\",\"reason_code\":\"SUCCESS\",\"reason\":\"\",\"data\":{\"headImage\":\"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLzyibBiaTFqdG9FgKtgxWKQSZyqNWYAAX0ypSae2CrfbRjf1o8PWCu9k5wtX3qqYqQVO8icUEV0CwHQ/132\",\"nickname\":\"XXX\",\"ticks\":4144,\"coin\":320,\"diamond\":0,\"doll\":0,\"lv\":1,\"vip\":4,\"vipTitle\":\"荣耀黄金\",\"amountSum\":610,\"amountTimes\":3,\"amountDiff\":390,\"token\":\"\",\"days\":0,\"isSignToday\":false,\"inviteCode\":\"JKh0h5hnhjhVhLhyt5\"}}"
	encode := bzstring.Utf8Encode(s)
	fmt.Println(encode)
}
