package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestFilter(t *testing.T) {
	zhName := "王小明"
	zhName = bzstring.FilterName(zhName)
	fmt.Println(zhName)

	zhName = "tom bob"
	zhName = bzstring.FilterName(zhName)
	fmt.Println(zhName)

	idNumber := "12345678910"
	idNumber = bzstring.FilterIDNumber(idNumber)
	fmt.Println(idNumber)

	s := "Hello World"
	index := bzstring.Index(s, "World")
	fmt.Println(index)
}
