package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestAscii(t *testing.T) {
	char := bzstring.AsciiChar(65)
	fmt.Println(char)

	num, err := bzstring.AsciiNum("A")
	fmt.Println(num, err)
}
