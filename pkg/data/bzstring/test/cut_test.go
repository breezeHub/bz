package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestCut(t *testing.T) {
	s := "golang is very nice"
	cut := bzstring.Cut(s, "ol", "c")
	fmt.Println(cut)

	substr := bzstring.Substr(s, 2, 6)
	fmt.Println(substr)
}
