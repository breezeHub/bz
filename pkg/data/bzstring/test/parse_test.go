package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestToInt(t *testing.T) {
	i, err := bzstring.ToInt("123")
	fmt.Println(i, err)
}

func TestToFloat(t *testing.T) {
	f, err := bzstring.ToFloat("123.123")
	fmt.Println(f, err)

	decimal := bzstring.ToDecimal(123.129123, 3)
	fmt.Println(decimal)
}
