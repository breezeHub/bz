package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

func TestString(t *testing.T) {
	// empty
	{
		notEmpty := bzstring.IsNotEmpty("abc")
		fmt.Println(notEmpty)

		notEmpty = bzstring.IsNotEmpty("")
		fmt.Println(notEmpty)

		empty := bzstring.IsEmpty("abc")
		fmt.Println(empty)

		empty = bzstring.IsEmpty("")
		fmt.Println(empty)
	}

	// repeat
	{
		repeatStr := bzstring.Repeat("bz框架 ", 3)
		fmt.Println(repeatStr)
	}

	// lower & upper
	{
		lower := bzstring.ToLower("ABC")
		fmt.Println(lower)

		isLower := bzstring.IsLower("A")
		fmt.Println(isLower)
		isLower = bzstring.IsLower("a")
		fmt.Println(isLower)

		lower = bzstring.ToLowerFirst("ABC")
		fmt.Println(lower)

		upper := bzstring.ToUpper("abc")
		fmt.Println(upper)

		isUpper := bzstring.IsUpper("A")
		fmt.Println(isUpper)
		isUpper = bzstring.IsUpper("a")
		fmt.Println(isUpper)

		upper = bzstring.ToUpperFirst("abc")
		fmt.Println(upper)

		upper = bzstring.ToUpperWords("hello world")
		fmt.Println(upper)
	}

	// count
	{
		count := bzstring.Count("breeze", "e")
		fmt.Println(count)

		count = bzstring.CountIgnore("BREEZE", "e")
		fmt.Println(count)

		countChar := bzstring.CountChar("breeze框架")
		fmt.Println(countChar)
	}

	{
		s := "hello"
		reverse := bzstring.Reverse(s)
		fmt.Println(reverse)
	}
}
