package bzstring

import (
	"strings"
)

// IsNotEmpty 判断字符串是否不为空
// IsNotEmpty(s string) bool
// @param s string 字符串
// @return bool 是否不为空
func IsNotEmpty(s string) bool {
	return len(s) > 0
}

// IsEmpty 判断字符串是否为空
// IsEmpty(s string) bool
// @param s string 字符串
// @return bool 是否为空
func IsEmpty(s string) bool {
	return len(s) <= 0
}

// Repeat 得到指定字符重复n次的字符串
// Repeat(s string, repeatTimes int) string
// @param s string 字符串
// @param repeatTimes int 重复次数
// @return string 结果字符串
func Repeat(s string, repeatTimes int) string {
	return strings.Repeat(s, repeatTimes)
}

// ToLower 将字符串转为小写
// ToLower(s string) string
// @param s string 字符串
// @return string 小写字符串结果
func ToLower(s string) string {
	return strings.ToLower(s)
}

// IsLower 判断字符串是否为小写
// IsLower(s string) bool
// @param s string 字符串
// @return bool 是否为小写
func IsLower(s string) bool {
	return ToLower(s) == s
}

// ToLowerFirst 将字符串首字符转为小写
// ToLowerFirst(s string) string
// @param s string 字符串
// @return string 首字符小写字符串结果
func ToLowerFirst(s string) string {
	if IsEmpty(s) {
		return s
	}

	r := []rune(s)
	return strings.ToLower(string(r[0])) + string(r[1:])
}

// ToUpper 将字符串转为大写
// ToUpper(s string) string
// @param s string 字符串
// @return string 大写字符串结果
func ToUpper(s string) string {
	return strings.ToUpper(s)
}

// IsUpper 判断字符串是否为大写
// IsUpper(s string) bool
// @param s string 字符串
// @return bool 是否为大写
func IsUpper(s string) bool {
	return ToUpper(s) == s
}

// ToUpperFirst 将字符串首字符转为大写
// ToUpperFirst(s string) string
// @param s string 字符串
// @return string 首字符大写字符串结果
func ToUpperFirst(s string) string {
	if IsEmpty(s) {
		return s
	}

	r := []rune(s)
	return strings.ToUpper(string(r[0])) + string(r[1:])
}

// ToUpperWords 将字符串每个单词首字符转为大写
// ToUpper(s string) string
// @param s string 字符串
// @return string 字符串结果
func ToUpperWords(s string) string {
	words := strings.Split(s, " ")
	if len(words) <= 0 {
		return ""
	}

	var str strings.Builder
	str.Grow(len(words)) // 预分配空间
	str.WriteString(ToUpperFirst(words[0]))
	for _, word := range words[1:] {
		str.WriteString(" ")
		str.WriteString(ToUpperFirst(word))
	}

	return str.String()
}

// Count 统计字符在字符串中出现的次数
// Count(s string, substr string) int
// @param s string 字符串
// @param substr string 统计字符
// @return int 出现的次数
func Count(s string, substr string) int {
	return strings.Count(s, substr)
}

// CountIgnore 统计字符在字符串中出现的次数，忽略大小写
// CountIgnore(s string, substr string) int
// @param s string 字符串
// @param substr string 统计字符
// @return int 出现的次数
func CountIgnore(s string, substr string) int {
	s = ToLower(s)
	return strings.Count(s, substr)
}

// CountChar 统计每个字符在字符串中出现的次数
// CountIgnore(s string) map[string]int
// @param s string 字符串
// @return map[string]int 字符和出现的次数
func CountChar(s string) map[string]int {
	r := []rune(s)
	m := make(map[string]int, 8)
	for _, item := range r {
		m[string(item)] = Count(s, string(item))
	}
	return m
}

func Contains(s, substr string) bool {
	return strings.Contains(s, substr)
}

func ContainsIgnore(s, substr string) bool {
	s = ToLower(s)
	substr = ToLower(substr)
	return strings.Contains(s, substr)
}

func Reverse(s string) string {
	r := []rune(s)
	length := len(r)

	for i := 0; i < length/2; i++ {
		r[i], r[length-1-i] = r[length-1-i], r[i]
	}
	return string(r)
}
