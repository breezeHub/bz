package bzvalidate

import "regexp"

// ZHName 验证真实姓名
func ZHName(name string) bool {
	// 使用正则表达式验证姓名，这里只是一个简单示例
	// 可根据实际需求调整正则表达式
	regex := "^[\u4e00-\u9fa5]{2,4}$"
	match, _ := regexp.MatchString(regex, name)
	return match
}

// IDNumber 验证身份证号码
func IDNumber(idNumber string) bool {
	// 使用正则表达式验证身份证号码，这里只是一个简单示例
	// 可根据实际需求调整正则表达式
	regex := "^[1-9]\\d{5}(19|20)\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])\\d{3}[\\dXx]$"
	match, _ := regexp.MatchString(regex, idNumber)
	return match
}

// Email 验证邮箱
func Email(email string) bool {
	regex := `^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`
	match, _ := regexp.MatchString(regex, email)
	return match
}
