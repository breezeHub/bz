package bzencry

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzint"
	"github.com/speps/go-hashids/v2"
)

var _hash, _ = hashids.New()

func EncodeShortId(i int) (string, error) {
	return _hash.EncodeHex(fmt.Sprintf("%09d", i))
}

func DecodeShortId(hash string) (int, error) {
	hex, err := _hash.DecodeHex(hash)
	if err != nil {
		return 0, err
	}
	return bzint.UnZeroFill(hex)
}
