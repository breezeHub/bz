package bzcode

import (
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
	"runtime/debug"
)

type Exception struct {
	message string
	stack   []byte
}

func (e *Exception) Message() string {
	return e.message
}

func (e *Exception) Stack() string {
	return bzbyte.ToString(e.stack)
}

// TryCatch 捕捉panic错误，防止服务退出，并提供error错误。go原生不支持try-catch语法，对于习惯try-catch的开发者来说很难受，虽然这是一种不好的习惯
// TryCatch(tryFn func(), catchFn func(err bzcode.Exception))
// @param tryFn func() 代码容器
// @param catchFn func(err bzcode.Exception) 代码容器中发生panic错误后返回的error message
func TryCatch(tryFn func(), catchFn func(err Exception)) {
	defer func() {
		if err := recover(); err != nil {
			if e, ok := err.(error); ok {
				catchFn(Exception{message: e.Error(), stack: debug.Stack()})
				return
			}
			catchFn(Exception{message: "unknown error", stack: debug.Stack()})
		}
	}()

	tryFn()
}
