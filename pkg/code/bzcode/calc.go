package bzcode

// Ternary 可以实现简单的条件判断功能。go原生不支持三元运算符，本函数可以提供类似的使用方式
// Ternary[T any](b bool, v1 T, v2 T) T
// @param b bool 条件：true时返回v1；false时返回v2
// @param v1 T 值1，条件为true时作为返回值
// @param v2 T 值2，条件为false时作为返回值
func Ternary[T any](b bool, v1 T, v2 T) T {
	if b {
		return v1
	}
	return v2
}
