package bzdump

import (
	"fmt"
	"os"
	"reflect"
)

// D 打印，等同fmt.Println
// D(a ...any)
// @param a ...any 打印内容
func D(a ...any) {
	_, _ = fmt.Println(a...)
}

// DD 断点格式化打印，等同fmt.Printf，打印后退出
// DD(a …any)
// @param a …any 打印内容
func DD(a ...any) {
	D(a...)
	os.Exit(0) // 状态码：0表示成功，非0表示出错
}

// VD 变量打印，会打印出变量类型和变量值
// VD(a any)
// @param a any 变量
func VD(a any) {
	_, _ = fmt.Printf("%s: %#v\n", reflect.TypeOf(a).String(), a)
}

// VDD 断点格式化打印，等同fmt.Printf，打印后退出
// VDD(a any)
// @param a any 打印内容
func VDD(a any) {
	VD(a)
	os.Exit(0)
}
