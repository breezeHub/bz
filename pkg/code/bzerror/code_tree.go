package bzerror

type ErrorCodeGroup map[string]error

type ErrorTreeGroup struct {
	em map[string]ErrorCodeGroup
}

func NewErrorTreeGroup() *ErrorTreeGroup {
	return &ErrorTreeGroup{
		em: make(map[string]ErrorCodeGroup),
	}
}

func (g *ErrorTreeGroup) AddCode(group string, code string, err error) {
	if _, ok := g.em[group]; !ok {
		g.em[group] = make(ErrorCodeGroup)
	}
	g.em[group][code] = err
}

func (g *ErrorTreeGroup) AddCodeGroup(em map[string]ErrorCodeGroup) {
	for group, m := range em {
		for code, err := range m {
			g.AddCode(group, code, err)
		}
	}
}

func (g *ErrorTreeGroup) Get(group string, code string) (error, bool) {
	err, ok := g.em[group][code]
	return err, ok
}

func (g *ErrorTreeGroup) Group(group string) (*ErrorCodeGroup, bool) {
	err, ok := g.em[group]
	return &err, ok
}

func (m *ErrorCodeGroup) AddCode(code string, err error) {
	(*m)[code] = err
}

func (m *ErrorCodeGroup) Get(code string) (error, bool) {
	err, ok := (*m)[code]
	return err, ok
}
