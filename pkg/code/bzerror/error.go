package bzerror

import "github.com/pkg/errors"

// New 生成一个错误，带堆栈信息
// New(message string) error
// @param message string 错误内容
// @return error 错误信息
func New(message string) error {
	return errors.New(message)
}

// Errorf 格式化生成一个错误
// Errorf(format string, args ...any) error
// @param format string 格式化错误内容
// @param args ...any 格式化参数
// @return error 错误信息
func Errorf(format string, args ...any) error {
	return errors.Errorf(format, args...)
}

// Wrap 包装一个错误
// Wrap(err error, message string) error
// @param err error 错误信息
// @param message string 补充内容
// @return error 错误信息
func Wrap(err error, message string) error {
	return errors.Wrap(err, message)
}

// WrapF 格式化包装一个错误
// WrapF(err error, format string, args ...any) error
// @param err error 错误信息
// @param format string 格式化补充内容
// @param args ...any 格式化参数
// @return error 错误信息
func WrapF(err error, format string, args ...any) error {
	return errors.Wrapf(err, format, args...)
}

// Unwrap 解包装一个错误
// Unwrap(err error) error
// @param err error 包装过的错误信息
// @return error 原错误信息
func Unwrap(err error) error {
	return errors.Unwrap(err)
}

// Is 判断一个错误是否与另一个错误相等
// Is(err error, target error) bool
// @param err error 错误信息
// @param target error 对比的错误信息
// @return bool 是否相等
func Is(err error, target error) bool {
	return errors.Is(err, target)
}

// As 将一个错误转换为另一个类型的错误
// As(err error, target any) bool
// @param err error 错误信息
// @param target error 需要转换成的错误信息
// @return bool 是否转换成功
func As(err error, target any) bool {
	return errors.As(err, target)
}

// Cause 返回最底层的根错误信息
// Cause(err error) error
// @param err error 错误信息
// @return err error 最底层的错误信息
func Cause(err error) error {
	return errors.Cause(err)
}

// WithStack 错误信息加上堆栈信息
// WithStack(err error) error
// @param err error 错误信息
// @return err error 加上堆栈信息的错误信息
func WithStack(err error) error {
	return errors.WithStack(err)
}
