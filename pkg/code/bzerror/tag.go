package bzerror

import (
	"fmt"
)

type TagError struct {
	tag string
}

func NewTagError(tag string) *TagError {
	return &TagError{tag: tag}
}

func (e *TagError) New(message string) error {
	return New(fmt.Sprintf("[%s] %s", e.tag, message))
}

func (e *TagError) Errorf(format string, args ...any) error {
	return Errorf(fmt.Sprintf("["+e.tag+"] "+format, args...))
}

func (e *TagError) Wrap(err error, message string) error {
	return Wrap(err, fmt.Sprintf("[%s] %s", e.tag, message))
}
