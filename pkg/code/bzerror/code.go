package bzerror

type ErrorGroup struct {
	em            map[string]error
	notFoundError error
}

func NewErrorGroup() *ErrorGroup {
	return &ErrorGroup{
		em:            make(map[string]error),
		notFoundError: New("UNKNOWN ERROR"),
	}
}

func (g *ErrorGroup) AddCode(code string, err error) {
	g.em[code] = err
}

func (g *ErrorGroup) AddCodeGroup(em map[string]error) {
	for code, err := range em {
		g.AddCode(code, err)
	}
}

func (g *ErrorGroup) GetAndExist(code string) (error, bool) {
	err, ok := g.em[code]
	return err, ok
}

func (g *ErrorGroup) Get(code string) error {
	err, ok := g.GetAndExist(code)
	if !ok {
		return g.notFoundError
	}
	return err
}
