package bzsql

import "strings"

func Case(field string, m map[string]string, elseString ...string) string {
	/*
		(
			CASE
				WHEN `type` = 'a' THEN '1'
				WHEN `type` = 'b' THEN '2'
				WHEN `type` = 'c' THEN '3'
			ELSE 'UNKNOWN' END
		)
	*/
	var (
		elseStr = "UNKNOWN"
		strBuff strings.Builder
	)

	if len(elseString) > 0 {
		elseStr = elseString[0]
	}

	strBuff.WriteString("( CASE ")
	for k, v := range m {
		strBuff.WriteString("WHEN `")
		strBuff.WriteString(field)
		strBuff.WriteString("`='")
		strBuff.WriteString(k)
		strBuff.WriteString("' THEN '")
		strBuff.WriteString(v)
		strBuff.WriteString("' ")
	}
	strBuff.WriteString(" ELSE '")
	strBuff.WriteString(elseStr)
	strBuff.WriteString("' END )")

	return strBuff.String()
}
