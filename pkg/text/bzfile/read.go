package bzfile

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
	"os"
	"strings"
)

func ReadStringTrim(filePath string) (string, error) {
	s, err := ReadString(filePath)
	return strings.TrimSpace(s), err
}

func ReadString(filePath string) (string, error) {
	bytes, err := ReadBytes(filePath)
	return bzbyte.ToString(bytes), err
}

func ReadBytes(filePath string) ([]byte, error) {
	if !IsExist(filePath) {
		return nil, fmt.Errorf("%s not exists", filePath)
	}

	if !IsFile(filePath) {
		return nil, fmt.Errorf("%s not file", filePath)
	}

	bytes, err := os.ReadFile(filePath)
	return bytes, err
}
