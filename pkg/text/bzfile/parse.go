package bzfile

import (
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"github.com/pkg/errors"
)

func ParseByFile(filePath string, m map[string]string, parseFn func(s string, m map[string]string) string) (string, error) {
	content, err := ReadString(filePath)
	if err != nil {
		return "", errors.Wrap(err, "获取基础模板文件异常")
	}

	if m == nil || len(m) <= 0 {
		return content, nil
	}

	if parseFn != nil {
		content = parseFn(content, m)
	}
	return content, nil
}

func ParseByFileWithBrace(filePath string, m map[string]string) (string, error) {
	return ParseByFile(filePath, m, bzstring.ReplaceAllWithBraceByMap)
}

func ParseByFileWithSquareBrace(filePath string, m map[string]string) (string, error) {
	return ParseByFile(filePath, m, bzstring.ReplaceAllWithSquareBraceByMap)
}
