package bzfile

import (
	"github.com/skip2/go-qrcode"
	"os"
	"path"
)

// GenQrcode 生成二维码
func GenQrcode(content, file string) error {
	if err := os.MkdirAll(path.Dir(file), os.ModePerm); err != nil {
		return err
	}

	return qrcode.WriteFile(content, qrcode.Medium, 256, file)
}
