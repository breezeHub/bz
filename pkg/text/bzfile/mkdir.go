package bzfile

import "os"

func Mkdir(path string) error {
	return os.MkdirAll(path, 0755)
}
