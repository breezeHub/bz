package bzyaml

import (
	"context"
	"os"
	"time"

	"gitee.com/breezeHub/bz/pkg/os/bzlock"
	"gitee.com/breezeHub/bz/pkg/os/bztime"
	"github.com/fsnotify/fsnotify"
	"github.com/google/uuid"
	"github.com/spf13/viper"
)

type yamlFile struct {
	viper *viper.Viper
	mtx   *bzlock.BzRWLocker

	filename string
}

func NewFile(filename string) *yamlFile {
	return &yamlFile{
		viper:    viper.New(),
		filename: filename,
		mtx:      bzlock.NewRW(filename + uuid.New().String()),
	}
}

// Load 根据conf路径读取内容
func (y *yamlFile) Load(config any) error {
	content, err := os.ReadFile(y.filename)
	if err != nil {
		return err
	}

	return load(content, config)
}

// LoadAndListen 根据conf路径读取内容，并监听更新错误
func (y *yamlFile) LoadAndListen(config any, changeFunc func(err error)) error {
	y.viper.SetConfigFile(y.filename)
	if err := y.readConfig(&config); err != nil {
		return err
	}

	y.viper.WatchConfig()
	y.viper.OnConfigChange(func(e fsnotify.Event) {
		err := y.readConfig(&config)
		if changeFunc != nil {
			changeFunc(err)
		}
	})
	return nil
}

func (y *yamlFile) LoadAndListenWithTicker(ctx context.Context, config any, t time.Duration, readFunc func(err error)) {
	y.viper.SetConfigFile(y.filename)

	tc := bztime.TickerNoExit(ctx, func(ctx context.Context) error {
		return y.readConfig(&config)
	}, t)

	go func() {
		for {
			select {
			case err := <-tc.C:
				if readFunc != nil {
					readFunc(err)
				}
			}
		}
	}()
}
