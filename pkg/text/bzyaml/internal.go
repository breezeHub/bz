package bzyaml

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// 根据io read读取配置文件后的字符串解析yaml
func load(s []byte, config any) error {
	return yaml.Unmarshal(s, config)
}

func (y *yamlFile) readConfig(config any) error {
	if err := y.viper.ReadInConfig(); err != nil {
		return err
	}

	y.mtx.Lock()
	defer func() { y.mtx.UnLock() }()

	if err := y.viper.Unmarshal(&config); err != nil {
		return errors.Wrap(err, "Unable to decode into struct")
	}
	return nil
}
