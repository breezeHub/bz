package bzrequest

type HeaderOption struct {
	Key   string
	Value string
}

var (
	JsonHeader = HeaderOption{Key: "Content-Type", Value: "application/json;charset=UTF-8"}
	FormHeader = HeaderOption{Key: "Content-Type", Value: "application/x-www-form-urlencoded;charset=UTF-8"}
	HtmlHeader = HeaderOption{Key: "Content-Type", Value: "text/html;charset=utf-8"}
	JsHeader   = HeaderOption{Key: "Content-Type", Value: "text/javascript;charset=utf-8"}
	JsxHeader  = HeaderOption{Key: "Content-Type", Value: "application/x-javascript"}
	CssHeader  = HeaderOption{Key: "Content-Type", Value: "text/css;charset=utf-8"}

	JpegHeader = HeaderOption{Key: "Content-Type", Value: "image/jpeg"}
	GifHeader  = HeaderOption{Key: "Content-Type", Value: "image/gif"}
	PngHeader  = HeaderOption{Key: "Content-Type", Value: "image/png"}
	SvgHeader  = HeaderOption{Key: "Content-Type", Value: "image/svg+xml"}
)

func (c *Curl) SetHeader(headers ...HeaderOption) *Curl {
	//设置请求头
	if len(headers) > 0 {
		for _, header := range headers {
			c.req.Header.Set(header.Key, header.Value)
		}
	}
	return c
}
