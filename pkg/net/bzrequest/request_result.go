package bzrequest

import (
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
	"gitee.com/breezeHub/bz/pkg/data/bzjson"
	"io"
	"net/http"
	"time"
)

func (c *Curl) Result(t time.Duration) ([]byte, error) {
	//抛出错误
	if c.Err != nil {
		return nil, c.Err
	}

	client := http.Client{Timeout: t}
	resp, err := client.Do(c.req) //发送请求
	if err != nil {
		return nil, err
	}

	//返回数据
	return io.ReadAll(resp.Body)
}

func (c *Curl) ResultString(t time.Duration) (string, error) {
	result, err := c.Result(t)
	if err == nil && result != nil {
		return bzbyte.ToString(result), nil
	}
	return "", err
}

func (c *Curl) ResultAny(t time.Duration, v any) error {
	result, err := c.Result(t)
	if err == nil && result != nil {
		return bzjson.DecodeAny(result, v)
	}
	return err
}

func (c *Curl) Close() error {
	return c.req.Body.Close()
}
