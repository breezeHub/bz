package bzrequest

import "time"

func Get(url string, data []byte, timeout time.Duration, headers ...HeaderOption) ([]byte, error) {
	req := New(GET, url, data)
	defer req.Close()

	req = req.SetHeader(headers...)
	return req.Result(timeout)
}

func GetString(url string, data []byte, timeout time.Duration, headers ...HeaderOption) (string, error) {
	req := New(GET, url, data)
	defer req.Close()

	req = req.SetHeader(headers...)
	return req.ResultString(timeout)
}

func GetAny(url string, data []byte, timeout time.Duration, v any, headers ...HeaderOption) error {
	req := New(GET, url, data)
	defer req.Close()

	req = req.SetHeader(headers...)
	return req.ResultAny(timeout, v)
}

func PostJson(url string, data []byte, timeout time.Duration, headers ...HeaderOption) ([]byte, error) {
	req := New(POST, url, data)
	defer req.Close()

	hs := append([]HeaderOption{JsonHeader}, headers...)
	req = req.SetHeader(hs...)
	return req.Result(timeout)
}

func PostJsonString(url string, data []byte, timeout time.Duration, headers ...HeaderOption) (string, error) {
	req := New(POST, url, data)
	defer req.Close()

	hs := append([]HeaderOption{JsonHeader}, headers...)
	req = req.SetHeader(hs...)
	return req.ResultString(timeout)
}

func PostJsonAny(url string, data []byte, timeout time.Duration, v any, headers ...HeaderOption) error {
	req := New(POST, url, data)
	defer req.Close()

	hs := append([]HeaderOption{JsonHeader}, headers...)
	req = req.SetHeader(hs...)
	return req.ResultAny(timeout, v)
}
