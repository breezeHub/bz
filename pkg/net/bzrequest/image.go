package bzrequest

import (
	"gitee.com/breezeHub/bz/pkg/text/bzfile"
	"time"
)

// GetHttpResult 获取网络资源
func GetHttpResult(url string, timeout time.Duration) ([]byte, error) {
	return Get(url, nil, timeout)
}

// DownloadHttpResult 存储网络资源
func DownloadHttpResult(url, filePath string, timeout time.Duration) error {
	bytes, err := Get(url, nil, timeout)
	if err != nil {
		return err
	}

	_, err = bzfile.WriteBytes(filePath, bytes)
	return err
}
