package bzrequest

import (
	"bytes"
	"net/http"
)

type Curl struct {
	req *http.Request
	Err error
}

type method struct {
	value string
}

var (
	GET     = method{value: "GET"}
	POST    = method{value: "POST"}
	PUT     = method{value: "PUT"}
	DELETE  = method{value: "DELETE"}
	HEAD    = method{value: "HEAD"}
	PATCH   = method{value: "PATCH"}
	CONNECT = method{value: "CONNECT"}
	OPTIONS = method{value: "OPTIONS"}
	TRACE   = method{value: "TRACE"}
)

func New(method method, url string, data []byte) *Curl {
	c := &Curl{} //请求结构体数据

	//创建request请求
	request, err := http.NewRequest(method.value, url, bytes.NewBuffer(data))
	if err != nil {
		c.Err = err
		return c
	}

	//成功创建请求结构体数据
	c.req = request
	return c
}
