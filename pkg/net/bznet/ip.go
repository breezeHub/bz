package bznet

import (
	"net"
	"strings"
)

// LocalIP 获取当前局域网IP地址
func LocalIP() (string, error) {
	conn, err := net.Dial("udp", "8.8.8.8:53")
	if err != nil {
		return "", err
	}
	defer conn.Close()

	return strings.Split(conn.LocalAddr().String(), ":")[0], nil
}
