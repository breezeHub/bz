package main

import "fmt"

// Factory 工厂接口
type Factory interface {
	FactoryMethod(owner string) Product
}

// ConcreteFactory 工厂类
type ConcreteFactory struct {
}

// FactoryMethod 工厂接口实现
func (c *ConcreteFactory) FactoryMethod(owner string) Product {
	return &ConcreteProduct{owner: owner}
}

// Product 产品接口
type Product interface {
	User()
}

// ConcreteProduct 产品类
type ConcreteProduct struct {
	owner string
}

func (c *ConcreteProduct) User() {
	fmt.Println("This is a concrete product - " + c.owner)
}

func main() {
	f := ConcreteFactory{}
	p := f.FactoryMethod("aaa")
	p.User()
}
