package main

import "fmt"

// 以口袋妖怪为例

// PocketMonster 口袋妖怪接口
type PocketMonster interface {
	SetSkill(skill string)
	GetName() string
	GetSkill() string
}

// Pikachu 皮卡丘
type Pikachu struct {
	name  string
	skill string
}

func NewPikachu() PocketMonster {
	return &Pikachu{name: "皮卡丘"}
}
func (p *Pikachu) SetSkill(skill string) {
	p.skill = skill
}
func (p *Pikachu) GetName() string {
	return p.name
}
func (p *Pikachu) GetSkill() string {
	return p.skill
}

// Charizard 喷火龙
type Charizard struct {
	name  string
	skill string
}

func NewCharizard() PocketMonster {
	return &Charizard{name: "喷火龙"}
}
func (p *Charizard) SetSkill(skill string) {
	p.skill = skill
}
func (p *Charizard) GetName() string {
	return p.name
}
func (p *Charizard) GetSkill() string {
	return p.skill
}

// PocketMonsterFactory 工厂
type PocketMonsterFactory struct {
}

func (f *PocketMonsterFactory) Get(name string) (PocketMonster, bool) {
	switch name {
	case "Pikachu":
		return NewPikachu(), true
	case "Charizard":
		return NewCharizard(), true
	default:
		return nil, false
	}
}

func main() {
	// 初始化工厂
	f := &PocketMonsterFactory{}

	// 使用工厂
	pcM, ok := f.Get("Pikachu")
	pcM.SetSkill("十万伏特")
	if ok {
		fmt.Printf("%s 使用了 %s\n", pcM.GetName(), pcM.GetSkill())
	}

	czM, ok := f.Get("Charizard")
	czM.SetSkill("喷射火焰")
	if ok {
		fmt.Printf("%s 使用了 %s\n", czM.GetName(), czM.GetSkill())
	}
}
