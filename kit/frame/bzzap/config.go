package bzzap

import "gopkg.in/natefinch/lumberjack.v2"

func getConfig(config *Config) *lumberjack.Logger {
	hook := lumberjack.Logger{
		Filename:   "./log/logger.log",
		MaxSize:    10,
		MaxBackups: 5,
		MaxAge:     7,
		Compress:   config.Compress,
	}
	if len(config.File) > 0 {
		hook.Filename = config.File
	}
	if config.MaxSize > 0 {
		hook.MaxSize = config.MaxSize
	}
	if config.MaxBackups > 0 {
		hook.MaxBackups = config.MaxBackups
	}
	if config.MaxAge > 0 {
		hook.MaxAge = config.MaxAge
	}

	return &hook
}
