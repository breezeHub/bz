package bzzap

import (
	"gitee.com/breezeHub/bz/pkg/data/bztime"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"time"
)

type Config struct {
	File          string
	MaxSize       int  // MB
	MaxBackups    int  // 保留旧文件的最大个数
	MaxAge        int  // days
	Compress      bool // 是否压缩 / 归档旧文件
	IsHideConsole bool // 日志是否在控制台展示
}

type Log struct {
	logger *zap.Logger

	IsHideConsole bool // 日志是否在控制台展示
}

func New(config *Config) *Log {
	z := &Log{}
	logHook := getConfig(config)
	if config.IsHideConsole {
		z.IsHideConsole = config.IsHideConsole
	}

	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.Format(bztime.DateTime))
	}

	coreArr := []zapcore.Core{
		zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderConfig),
			zapcore.AddSync(logHook),
			zap.InfoLevel,
		),
		zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderConfig),
			zapcore.Lock(os.Stderr),
			zap.ErrorLevel,
		),
	}
	if !config.IsHideConsole {
		coreArr = append(coreArr, zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderConfig),
			zapcore.Lock(os.Stdout),
			zap.DebugLevel,
		))
	}

	core := zapcore.NewTee(coreArr...)
	z.logger = zap.New(core)
	return z
}

func (l *Log) Info(tag, msg string, err error) {
	l.logger.Info("["+tag+"] "+msg, zap.Error(err))
}

func (l *Log) Error(tag, msg string, err error) {
	l.logger.Error("["+tag+"] "+msg, zap.Error(err))
}

func (l *Log) Fatal(tag, msg string, err error) {
	l.logger.Fatal("["+tag+"] "+msg, zap.Error(err))
}

func (l *Log) Debug(tag, msg string, err error) {
	l.logger.Debug("["+tag+"] "+msg, zap.Error(err))
}

func (l *Log) Warn(tag, msg string, err error) {
	l.logger.Warn("["+tag+"] "+msg, zap.Error(err))
}

func (l *Log) Panic(tag, msg string, err error) {
	l.logger.Panic("["+tag+"] "+msg, zap.Error(err))
}

func (l *Log) Sync() error {
	return l.logger.Sync()
}
