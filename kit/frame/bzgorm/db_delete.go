package bzgorm

import "gorm.io/gorm"

func DeleteById[T any](db *gorm.DB, ids ...uint) (int64, error) {
	res := db.Delete(new(T), ids)
	return res.RowsAffected, res.Error
}
