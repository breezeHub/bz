package bzgorm

import (
	"fmt"
	"gorm.io/gorm"
)

/* ------------------ Find ------------------ */

func Find[T any](db *gorm.DB, fn func(tx *gorm.DB) *gorm.DB) ([]T, error) {
	var list []T
	obj := db.Model(new(T))
	obj = fn(obj)

	err := obj.Find(&list).Error
	return list, err
}

func Last[T any](db *gorm.DB, fn func(tx *gorm.DB) *gorm.DB) (T, error) {
	var list T
	obj := db.Model(new(T))
	obj = fn(obj)

	err := obj.Last(&list).Error
	return list, err
}

/* ------------------ Sum ------------------ */

type sum[V sumType] struct {
	Sum V
}

type sumType interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 | ~float32 | ~float64
}

func Sum[T any, V sumType](db *gorm.DB, field string, fn func(tx *gorm.DB) *gorm.DB) ([]*sum[V], error) {
	var list []*sum[V]
	obj := db.Model(new(T)).Select(fmt.Sprintf("SUM(%s) as `sum`", field))

	obj = fn(obj)

	err := obj.Find(&list).Error
	return list, err
}

type count struct {
	Count int64
}

func Count[T any](db *gorm.DB, fn func(tx *gorm.DB) *gorm.DB) ([]*count, error) {
	var c []*count
	obj := db.Model(new(T)).Select("count(*) AS count")
	obj = fn(obj)

	err := obj.Find(&c).Error
	return c, err
}
