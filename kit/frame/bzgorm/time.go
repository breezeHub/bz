package bzgorm

import (
	"database/sql/driver"
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"time"
)

// Time format json time field by myself
type Time struct {
	time.Time
}

// MarshalJSON on JSONTime format Time field with %Y-%m-%d %H:%M:%S
func (t Time) MarshalJSON() ([]byte, error) {
	var format string
	if (t != Time{}) {
		format = t.Format(time.DateTime)
	}

	formatted := fmt.Sprintf("\"%s\"", format)
	return bzstring.ToByte(formatted), nil
}

// Value insert timestamp into mysql need this function.
func (t Time) Value() (driver.Value, error) {
	var zeroTime time.Time

	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}

	return t.Time, nil
}

// Scan valueof time.Time
func (t *Time) Scan(v interface{}) error {
	if value, ok := v.(time.Time); ok {
		*t = Time{Time: value}
		return nil
	}

	return fmt.Errorf("can not convert %v to timestamp", v)
}
