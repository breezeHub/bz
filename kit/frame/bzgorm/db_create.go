package bzgorm

import "gorm.io/gorm"

func Create(db *gorm.DB, value any, batchSize ...int) (int64, error) {
	var result *gorm.DB

	if len(batchSize) >= 0 {
		result = db.CreateInBatches(value, batchSize[0])
	} else {
		result = db.Create(value)
	}
	return result.RowsAffected, result.Error
}
