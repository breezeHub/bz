package bzgorm

import "gorm.io/gorm"

// FirstById 根据主键检索
func FirstById[T any](db *gorm.DB, id uint) (T, error) {
	var v T
	err := db.First(&v, id).Error
	return v, err
}

func FirstByWhere[T any](db *gorm.DB, where string, args ...any) (T, error) {
	var v T
	err := db.First(&v, byWhereAndArgs(where, args)...).Error
	return v, err
}

// LastById 根据主键检索
func LastById[T any](db *gorm.DB, id uint) (T, error) {
	var v T
	err := db.Last(&v, id).Error
	return v, err
}

func LastByWhere[T any](db *gorm.DB, where string, args ...any) (T, error) {
	var v T
	err := db.Last(&v, byWhereAndArgs(where, args)...).Error
	return v, err
}

// OneById 根据主键检索
func OneById[T any](db *gorm.DB, id uint) (T, error) {
	var v T
	err := db.Take(&v, id).Error
	return v, err
}

func OneByWhere[T any](db *gorm.DB, where string, args ...any) (T, error) {
	var v T
	err := db.Take(&v, byWhereAndArgs(where, args)...).Error
	return v, err
}

func FindById[T any](db *gorm.DB, ids ...uint) ([]T, error) {
	var v []T
	err := db.Find(&v, ids).Error
	return v, err
}

func FindByWhere[T any](db *gorm.DB, where string, args ...any) ([]T, error) {
	var v []T
	err := db.Find(&v, byWhereAndArgs(where, args)...).Error
	return v, err
}

func FindInIds[T any, I uint | uint32 | uint64 | int | int32 | int64 | string](db *gorm.DB, ids []I) ([]T, error) {
	var v []T
	err := db.Where(ids).Find(&v).Error
	return v, err
}

func LeftJoin() {

}
