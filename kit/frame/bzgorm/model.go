package bzgorm

import (
	"gorm.io/gorm"
)

type Model struct {
	Id        uint           `gorm:"column:id;primarykey" json:"id"`
	CreatedAt Time           `gorm:"column:created_at" json:"createdAt"`
	UpdatedAt Time           `gorm:"column:updated_at" json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at;index" json:"deletedAt"`
}
