package bzgorm

import (
	"gitee.com/breezeHub/bz/pkg/data/bzvalue"
	"gorm.io/gorm"
)

// Page 通用分页
// @Description 通用分页查条件
type Page struct {
	Page     *int   `json:"page" form:"page" bind:"numeric"`         // 当前页，默认为1
	PageSize *int   `json:"pageSize",form:"pageSize" bind:"numeric"` // 分页条目数据，默认10
	Total    *int64 `json:"total"`                                   // 查询总数量
}

// Pages 分页查询返回
// @Description 分页查询返回
type Pages struct {
	Page Page `json:"pages"` // 分页信息
	Data any  `json:"data"`  // 返回数据
}

func (p *Page) Data(data any) *Pages {
	pages := Pages{Page: *p, Data: data}
	return &pages
}

// Paginate Usage: db.Scopes(Page(r)).Find(&users)
func Paginate(db *gorm.DB, page *Page, fn func(tx *gorm.DB) *gorm.DB) *gorm.DB {
	if *page.Page == 0 {
		page.Page = bzvalue.Pointer[int](1)
	}

	offset := (*page.Page - 1) * *page.PageSize

	var total int64
	fn(db).Count(&total)
	page.Total = &total

	return db.Offset(offset).Limit(*page.PageSize)
}

func FindByPaginate[T any](db *gorm.DB, page int, pageSize int, fn func(tx *gorm.DB) *gorm.DB) (*Pages, error) {
	pageData := &Page{
		Page:     &page,
		PageSize: &pageSize,
	}

	orders, err := Find[T](db,
		func(tx *gorm.DB) *gorm.DB {
			return Paginate(tx, pageData, func(tx1 *gorm.DB) *gorm.DB {
				return fn(tx1)
			})
		})
	if err != nil {
		return nil, err
	}

	return pageData.Data(orders), err
}
