package bzgorm

import (
	"database/sql/driver"
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"time"
)

// Day format json time field by myself
type Day struct {
	time.Time
}

// MarshalJSON on JSONTime format Time field with %Y-%m-%d %H:%M:%S
func (t Day) MarshalJSON() ([]byte, error) {
	var format string
	if (t != Day{}) {
		format = t.Format(time.DateOnly)
	}

	formatted := fmt.Sprintf("\"%s\"", format)
	return bzstring.ToByte(formatted), nil
}

// Value insert timestamp into mysql need this function.
func (t Day) Value() (driver.Value, error) {
	var zeroTime time.Time

	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}

	return t.Time, nil
}

// Scan valueof time.Time
func (t *Day) Scan(v interface{}) error {
	if value, ok := v.(time.Time); ok {
		*t = Day{Time: value}
		return nil
	}

	return fmt.Errorf("can not convert %v to timestamp", v)
}
