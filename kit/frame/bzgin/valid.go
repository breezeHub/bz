package bzgin

import (
	"gitee.com/breezeHub/bz/pkg/code/bzerror"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"reflect"
)

type ValidateErrFunc func(ctx *gin.Context, err error)

func ValidateQuery[T any](f func(ctx *gin.Context, param *T), errFunc ...ValidateErrFunc) func(ctx *gin.Context) {
	if len(errFunc) <= 0 {
		errFunc = []ValidateErrFunc{func(ctx *gin.Context, err error) {
			BadRequestResponse(ctx, "请求参数错误", err.Error())
		}}
	}

	return func(ctx *gin.Context) {
		var param T
		if err := ctx.ShouldBindQuery(&param); err != nil {
			for _, ef := range errFunc {
				ef(ctx, err)
			}
			return
		}
		f(ctx, &param)
	}
}

func ValidateJson[T any](f func(ctx *gin.Context, param *T), errFunc ...ValidateErrFunc) func(ctx *gin.Context) {
	if len(errFunc) <= 0 {
		errFunc = []ValidateErrFunc{func(ctx *gin.Context, err error) {
			BadRequestResponse(ctx, "请求参数错误", err.Error())
		}}
	}

	return func(ctx *gin.Context) {
		var param T
		if err := ctx.ShouldBindJSON(&param); err != nil {
			for _, ef := range errFunc {
				ef(ctx, err)
			}
			return
		}
		f(ctx, &param)
	}
}

func ValidateQueryWithFilter[T any](f func(ctx *gin.Context, param *T), errFunc ...ValidateErrFunc) func(ctx *gin.Context) {
	if len(errFunc) <= 0 {
		errFunc = []ValidateErrFunc{func(ctx *gin.Context, err error) {
			BadRequestResponse(ctx, "请求参数错误", err.Error())
		}}
	}

	return func(ctx *gin.Context) {
		var param T
		if err := ctx.ShouldBindQuery(&param); err != nil {
			err = FilterBindErr[T](err)
			for _, ef := range errFunc {
				ef(ctx, err)
			}
			return
		}
		f(ctx, &param)
	}
}

func ValidateJsonWithFilter[T any](f func(ctx *gin.Context, param *T), errFunc ...ValidateErrFunc) func(ctx *gin.Context) {
	if len(errFunc) <= 0 {
		errFunc = []ValidateErrFunc{func(ctx *gin.Context, err error) {
			BadRequestResponse(ctx, "请求参数错误", err.Error())
		}}
	}

	return func(ctx *gin.Context) {
		var param T
		if err := ctx.ShouldBindJSON(&param); err != nil {
			err = FilterBindErr[T](err)
			for _, ef := range errFunc {
				ef(ctx, err)
			}
			return
		}
		f(ctx, &param)
	}
}

// FilterBindErr 自定义错误消息
func FilterBindErr[T any](err error) error {
	if err == nil {
		return nil
	}

	errs,ok:=err.(validator.ValidationErrors)
	if !ok {
		return err
	}

	var r T
	s := reflect.TypeOf(r)
	for _, fieldError := range errs {
		filed, _ := s.FieldByName(fieldError.Field()) //用反射获取参数名

		//获取对应binding得错误消息 tag：错误类型_err，如：required_err:"userId不能为空"`
		if errTagText := filed.Tag.Get(fieldError.Tag() + "_err"); errTagText != "" {
			return bzerror.New(errTagText)
		}

		//通用错误类型 tag：err，如：err:"userId错误"`
		if errText := filed.Tag.Get("err"); errText != "" {
			return bzerror.New(errText)
		}

		//无法匹配到错误，如：user的格式需遵守: required
		return bzerror.New(fieldError.Field() + "的格式需遵守: " + fieldError.Tag())
	}
	return err
}
