package bzgin

import (
	"gitee.com/breezeHub/bz/pkg/code/bzcode"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"github.com/gin-gonic/gin"
	"strconv"
)

func CtxGet[T any](ctx *gin.Context, key string) (T, bool) {
	var t T

	value, exists := ctx.Get(key)
	if !exists {
		return t, false
	}

	t, ok := value.(T)
	return t, ok
}

func CtxGetQueryInt(ctx *gin.Context, key string) int {
	iString := ctx.Query(key)
	iInt, _ := strconv.Atoi(iString)
	return iInt
}

func CtxGetQueryIntOrDefault(ctx *gin.Context, key string, def int) int {
	iDef := strconv.Itoa(def)

	iString := ctx.DefaultQuery(key, iDef)
	if bzstring.IsEmpty(iString) {
		return def
	}
	iInt, _ := strconv.Atoi(iString)
	return iInt
}

func CtxGetQueryInt64(ctx *gin.Context, key string) int64 {
	iString := ctx.Query(key)
	iInt, _ := strconv.ParseInt(iString, 10, 64)
	return iInt
}

func CtxGetQueryInt64OrDefault(ctx *gin.Context, key string, def int64) int64 {
	iDef := strconv.FormatInt(def, 10)

	iString := ctx.DefaultQuery(key, iDef)
	if bzstring.IsEmpty(iString) {
		return def
	}
	iInt, _ := strconv.ParseInt(iString, 10, 64)
	return iInt
}

func IsHttps(ctx *gin.Context) bool {
	return ctx.Request.TLS != nil
}

func GetHost(ctx *gin.Context) string {
	return bzcode.Ternary(IsHttps(ctx), "https://", "http://") + ctx.Request.Host
}
