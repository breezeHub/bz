package bzgin

import (
	"gitee.com/breezeHub/bz/pkg/data/bzslice"
	"github.com/gin-gonic/gin"
	"net/http"
)

// Cors 跨域
func Cors(urls ...string) gin.HandlerFunc {
	return func(c *gin.Context) {

		if c.Request.Header.Get("Origin") != "" {
			host := c.Request.Header.Get("Referer")

			var url string
			if len(urls) > 0 {
				if bzslice.Contain[string](urls, "*") {
					url = "*"
				} else {
					if bzslice.Contain[string](urls, host) {
						url = host
					}
				}
			} else {
				url = "*"
			}

			c.Header("AccessControl-Allow-Origin", url) // 可将将 * 替换为指定的域名
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE,UPDATE")
			c.Header("Access-Control-Allow-Headers", "Authorization, Content-Length, X-CSRF-Token, Token, session")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
		}

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}

		c.Next()
	}
}
