//go:build wireinject
// +build wireinject

package gen

import "github.com/google/wire"

func AppInit() *App { panic(wire.Build(set)) }
