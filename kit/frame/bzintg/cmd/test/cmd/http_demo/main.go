package main

import (
	"test/cmd/http_demo/gen"
)

func main() {
	app := gen.AppInit()
	app.Start()
}

