package repository

import (
	"test/internal/domain"
	"time"

	"github.com/patrickmn/go-cache"
)

var _ domain.MemberRepo = (*MemberCache)(nil)

type MemberCache struct {
	cache *cache.Cache
}

func NewMemberCache() domain.MemberRepo {
	c := &MemberCache{
		cache: cache.New(5*time.Minute, 10*time.Minute), //默认5分钟过期，1分钟扫描一次缓存Key
	}
	c.cache.Set("name", "breeze", 10*time.Hour)
	return c
}

func (c *MemberCache) GetName() string {
	val, ok := c.cache.Get("name")
	if ok {
		return val.(string)
	}
	return ""
}
