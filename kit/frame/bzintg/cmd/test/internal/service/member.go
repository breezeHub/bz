package service

import (
	"test/internal/domain"
	"test/internal/service/dto"
	"gitee.com/breezeHub/bz/kit/frame/bzgin"
	"github.com/gin-gonic/gin"
)

type MemberService struct {
	memberDomain *domain.MemberDomain
}

func NewMemberService(memberDomain *domain.MemberDomain) *MemberService {
	return &MemberService{
		memberDomain: memberDomain,
	}
}

func (s *MemberService) GetInfoById(c *gin.Context, param *dto.GetInfoByIdRequest) {
	name, err := s.memberDomain.GetInfoById(param.Id)
	if err != nil {
		bzgin.ErrorResponse(c, "查询用户失败", err.Error())
	}
	bzgin.SuccessResponse(c, map[string]string{"name": name})
}
