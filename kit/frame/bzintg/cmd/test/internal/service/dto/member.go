package dto

type GetInfoByIdRequest struct {
    Id int `form:"id" binding:"required"`
}
