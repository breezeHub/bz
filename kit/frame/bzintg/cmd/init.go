package main

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/code/bzcode"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"gitee.com/breezeHub/bz/pkg/os/bzcmd"
	"gitee.com/breezeHub/bz/pkg/text/bzfile"
	"golang.org/x/sync/errgroup"
	"os"
	"os/exec"
	"strings"
)

type App struct {
	Project string
}

func (a *App) install() {
	out, err := bzcmd.CmdOutput("go", "get", "-u", "github.com/google/wire")
	if err != nil {
		fmt.Println(out)
	}
	a.panic(err)

	out, err = bzcmd.CmdOutput("go", "install", "github.com/google/wire/cmd/wire@latest")
	if err != nil {
		fmt.Println(out)
	}
	a.panic(err)
}

func (a *App) initProject() {
	err := bzfile.Mkdir(a.Project)
	a.panic(err)

	cmd := exec.Command("go", "mod", "init", a.Project)
	cmd.Dir = a.Project
	out, err := cmd.Output()
	if err != nil {
		fmt.Println(out)
	}
	a.panic(err)
}

func (a *App) createDir() {
	fileArray := []string{
		"/cmd/http_demo/gen",
		"/internal/domain",
		"/internal/repository",
		"/internal/server",
		"/internal/service/dto",
	}

	eg := errgroup.Group{}
	for _, file := range fileArray {
		file := file
		eg.Go(func() error { return bzfile.Mkdir(a.Project + file) })
	}
	a.panic(eg.Wait())
}

var fileArray = [8]string{
	"/cmd/http_demo/main.go",
	"/cmd/http_demo/gen/app.go",
	"/cmd/http_demo/gen/wire.go",

	"/internal/server/http.go",
	"/internal/service/member.go",
	"/internal/service/dto/member.go",
	"/internal/domain/member.go",
	"/internal/repository/member_cache.go",
}

func (a *App) genFile() {
	m := map[string]string{
		"Project": a.Project,
	}

	eg := errgroup.Group{}
	for _, file := range fileArray {
		file := file
		eg.Go(func() error {
			tpl := bzstring.ReplaceAllByMap(file, map[string]string{
				"/": "_", ".go": ".tpl",
			})
			content, err := bzfile.ReadString("template/" + tpl[1:])
			if err != nil {
				return err
			}

			c := bzstring.ReplaceAllWithBraceByMap(content, m)
			_, err = bzfile.WriteString(a.Project+file, c)
			return err
		})
	}

	a.panic(eg.Wait())
}

func (a *App) build() {
	cmd := exec.Command("go", "mod", "tidy")
	cmd.Dir = a.Project
	out, err := cmd.Output()
	if len(strings.Trim(string(out), "")) > 0 {
		fmt.Println(string(out))
	}
	a.panic(err)

	cmd = exec.Command("wire")
	cmd.Dir = a.Project + "/cmd/http_demo/gen"
	out, err = cmd.Output()
	if len(strings.Trim(string(out), "")) > 0 {
		fmt.Println(string(out))
	}
	a.panic(err)
}

func (a *App) panic(err error) {
	if err != nil {
		_ = os.RemoveAll(a.Project)
	}
	bzcode.IfErrPanic(err)
}
