package main

import "log"

func main() {
	project := "test"

	app := &App{Project: project}

	// 安装环境
	log.Println("安装环境开始")
	app.install()
	log.Println("安装环境完成")

	log.Println("初始化项目开始")
	app.initProject()
	log.Println("初始化项目完成")

	// 创建目录
	log.Println("创建必要目录开始")
	app.createDir()
	log.Println("创建必要目录完成")

	// 创建文件
	log.Println("创建demo文件开始")
	app.genFile()
	log.Println("创建demo文件完成")

	log.Println("构建项目开始")
	app.build()
	log.Println("构建项目完成")
}
