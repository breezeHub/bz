package main

import (
	"{{ Project }}/cmd/http_demo/gen"
)

func main() {
	app := gen.AppInit()
	app.Start()
}

