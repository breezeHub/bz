package gen

import (
	"context"
	"{{ Project }}/internal/domain"
	"{{ Project }}/internal/repository"
	"{{ Project }}/internal/server"
	"{{ Project }}/internal/service"

	"gitee.com/breezeHub/bz/pkg/code/bzcode"
	"gitee.com/breezeHub/bz/pkg/os/bzsched"
	"github.com/google/wire"
)

var set = wire.NewSet(
	NewApp,

	// server
	wire.NewSet(server.NewHttpServer),

	// service
	wire.NewSet(service.NewMemberService),

	// domain
	wire.NewSet(domain.NewMemberDomain),

	// repository
	wire.NewSet(repository.NewMemberCache),
)

type App struct {
	httpServer *server.HttpServer
}

func NewApp(httpServer *server.HttpServer) *App {
	return &App{
		httpServer: httpServer,
	}
}

func (a *App) Start() {
	s := bzsched.New(context.Background())
	s.Add("http-demo", func(ctx context.Context) error {
		return a.httpServer.Start()
	})
	err := s.Run()
	bzcode.IfErrPanic(err)
}
