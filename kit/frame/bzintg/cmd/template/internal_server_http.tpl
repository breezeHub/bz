package server

import (
	"{{ Project }}/internal/service"
	"gitee.com/breezeHub/bz/kit/frame/bzgin"
	"github.com/gin-gonic/gin"
)

type HttpServer struct {
	Engine *gin.Engine
}

func NewHttpServer(memberService *service.MemberService) *HttpServer {
	r := gin.Default()
	r.GET("", bzgin.ValidateQueryWithFilter(memberService.GetInfoById))

	return &HttpServer{Engine: r}
}

func (s *HttpServer) Start() error {
	return s.Engine.Run(":2256")
}
