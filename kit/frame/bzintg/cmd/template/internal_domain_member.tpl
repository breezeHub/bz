package domain

type MemberDomain struct {
	memberCache MemberRepo
}

type MemberRepo interface {
	GetName() string
}

func NewMemberDomain(memberCache MemberRepo) *MemberDomain {
	return &MemberDomain{
		memberCache: memberCache,
	}
}

func (s *MemberDomain) GetInfoById(id int) (string, error) {
	name := s.memberCache.GetName()
	return name, nil
}
