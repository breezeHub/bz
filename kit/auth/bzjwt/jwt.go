package bzjwt

import (
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"github.com/dgrijalva/jwt-go"
	"time"
)

type Claims[T any] struct {
	Data T
	jwt.StandardClaims
}

func GenToken[T any](signingKey string, f func(claims *Claims[T]) *Claims[T], expireTime time.Duration) (string, error) {
	t := time.Now().Add(expireTime)
	claims := &Claims[T]{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: t.Unix(), //过期时间
			IssuedAt:  time.Now().Unix(),
		},
	}
	claims = f(claims)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(bzstring.ToByte(signingKey))
}

func Parse[T any](signingKey, tokenString string) (bool, *Claims[T], error) {
	claims := &Claims[T]{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (i any, err error) {
		return bzstring.ToByte(signingKey), nil
	})

	if err != nil {
		return false, claims, err
	}
	return token.Valid, claims, err
}
