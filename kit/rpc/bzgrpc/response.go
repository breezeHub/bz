package bzgrpc

const MsgIDErrorResponse = 0
const MsgIDNoResponse = 10000

func Response(msgID uint32, data []byte) (uint32, []byte, error) {
	return msgID, data, nil
}

func NoResponse() (uint32, []byte, error) {
	return MsgIDNoResponse, nil, nil
}

func ErrorResponse(err error) (uint32, []byte, error) {
	return MsgIDErrorResponse, nil, err
}
