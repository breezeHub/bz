package bzgrpc

import (
	"gitee.com/breezeHub/bz/kit/frame/bzzap"
)

type BizLog struct {
	logger *bzzap.Log
	typ    string
}

func WrapLog(logger *bzzap.Log, typ string) *BizLog {
	return &BizLog{logger: logger, typ: typ}
}

func (l *BizLog) Info(msg string, err error) {
	l.logger.Info(l.typ, "INFO "+msg, err)
}

func (l *BizLog) Error(msg string, err error) {
	l.logger.Error(l.typ, "ERROR "+msg, err)
}

func (l *BizLog) Fatal(msg string, err error) {
	l.logger.Fatal(l.typ, "FATAL "+msg, err)
}
func (l *BizLog) Debug(msg string, err error) {
	l.logger.Debug(l.typ, "DEBUG "+msg, err)
}
func (l *BizLog) Warn(msg string, err error) {
	l.logger.Warn(l.typ, "WARN "+msg, err)
}
func (l *BizLog) Panic(msg string, err error) {
	l.logger.Panic(l.typ, "PANIC "+msg, err)
}
