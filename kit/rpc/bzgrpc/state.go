package bzgrpc

import (
	"context"
	"gitee.com/breezeHub/bz/kit/rpc/bzgrpc/protofile"
	"gitee.com/breezeHub/bz/pkg/data/bzgncmap"
)

type IPState struct {
	EndTime  int64 // 过期时间戳
	IP       string
	RespChan chan *protofile.LogicResponse
	Cancel   context.CancelFunc
	//IsNotFirst bool
	Info       any
	WriteTimer int64
}

var fsm = bzgncmap.New(
	bzgncmap.InitCap[string, *IPState](20),
)

func FSM() *bzgncmap.Map[string, *IPState] {
	return fsm
}
