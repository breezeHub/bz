package bzgrpc

import (
	"context"
	"fmt"
	"gitee.com/breezeHub/bz/kit/frame/bzzap"
	protofile "gitee.com/breezeHub/bz/kit/rpc/bzgrpc/protofile"
	"gitee.com/breezeHub/bz/pkg/os/bztime"
	"github.com/gogo/protobuf/proto"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net"
	"sync"
	"time"
)

type Service[SI any, NI any] struct {
	protofile.UnimplementedLogicServiceServer

	routerMap        RouterMap                 // 请求路由
	noticeArray      map[uint32]NoticeFunc[NI] // 通知路由
	hearBeatTicker   time.Duration
	hearBeatTimeout  time.Duration
	isClientHearBeat bool // 是否依赖客户端心跳续约
	info             SI   // 服务附带的信息
	logger           *BizLog
}

// New 服务
// SI server info 服务附带的信息
// NC notice channel 通知的主结构
// NI notice info 通知附带的信息
func New[SI any, NI any](
	address string,
	routerMap RouterMap, fs map[uint32]NoticeFunc[NI],
	hearBeatTicker time.Duration, hearBeatTimeout time.Duration, isClientHearBeat bool,
	info SI,
) error {
	logger := bzzap.New(&bzzap.Config{File: "./log/__bz_grpc.log", MaxSize: 100, MaxBackups: 5, MaxAge: 7})
	defer logger.Sync()

	log := WrapLog(logger, "Bz GRPC Server")

	srv := &Service[SI, NI]{
		routerMap:        routerMap,
		noticeArray:      fs,
		hearBeatTicker:   hearBeatTicker,
		hearBeatTimeout:  hearBeatTimeout,
		isClientHearBeat: isClientHearBeat,
		info:             info,
		logger:           log,
	}

	// 创建并启动 gRPC 服务
	listen, err := net.Listen("tcp", address)
	if err != nil {
		log.Error("监听服务失败", err)
		return err
	}

	s := grpc.NewServer()
	protofile.RegisterLogicServiceServer(s, srv) // 注册服务

	if err = s.Serve(listen); err != nil {
		log.Error("启动服务失败", err)
		return err
	}
	return nil
}

func (s *Service[SI, NI]) Send(ss protofile.LogicService_SendServer) error {
	ssCtx, cancel := context.WithCancel(ss.Context())

	eg, egCtx := errgroup.WithContext(ssCtx)

	var (
		noticeChan     = make(chan string, 1)
		sendNoticeChan = make(chan string, 1)
		hearBeatChan   = make(chan string, 1)
		noticeOnce     sync.Once
		sendNoticeOnce sync.Once
	)

	// 心跳数据
	heartBeat, _ := proto.Marshal(&protofile.HeartBeatToC{})

	// 心跳检测
	eg.Go(func() error {
		ip := <-hearBeatChan

		// 通知启动通知
		//noticeChan <- ip
		//sendNoticeChan <- ip

		return bztime.Ticker(egCtx, func(ctx context.Context) error {
			// 心跳检测
			if err := ss.SendMsg(&protofile.LogicResponse{MsgID: 0, Data: heartBeat}); err != nil {
				err = errors.Wrap(err, "发送心跳失败")
				s.logger.Error(fmt.Sprintf("IP:%s 心跳检测错误", ip), err)
				return err
			}
			s.logger.Info(fmt.Sprintf("IP:%s 心跳检测正常", ip), nil)

			// 更新状态机
			if !s.isClientHearBeat {
				_ = fsm.ExistPut(ip, func(value **IPState) {
					(*value).EndTime = time.Now().Add(s.hearBeatTicker + s.hearBeatTimeout).UnixNano()
				})
			}

			return nil
		}, s.hearBeatTicker)
	})

	// 启动通知（生产者），notice 子任务 ctx 可以 cancel
	eg.Go(func() error {
		ip := <-noticeChan

		noticeOnce.Do(func() {
			for {
				if _, ok := fsm.Get(ip); ok {
					break
				}
				time.Sleep(100 * time.Microsecond)
			}
			info, _ := fsm.Get(ip)

			// 状态通知启动，阻塞
			if i, ok := info.Info.(NI); ok {
				s.Notice(egCtx, ip, i)
			}
		})
		return nil
	})

	// 发送通知（消费者），ctx 可以 cancel
	eg.Go(func() error {
		ip := <-sendNoticeChan

		errC := make(chan error, 1)
		sendNoticeOnce.Do(func() {
			// 状态通知
			errC <- s.SendNotice(egCtx, ip, ss)
		})
		return <-errC
	})

	// 处理请求 && 响应
	eg.Go(func() error {
		return bztime.Ticker(egCtx, func(ctx context.Context) error {
			// 接收数据
			req, err := ss.Recv()
			if req != nil {
				s.logger.Info(fmt.Sprintf("[MsgID: %d, IP: %s] 接收的数据: Data: %v", req.MsgID, req.IP, req.Data), err)
			}

			// 验证数据
			if err = checkMsg(req, err); err != nil {
				s.logger.Error(fmt.Sprintf("[req: %#v] 验证请求的数据错误", req), err)
				return err
			}

			// 维护机器活跃列表
			chanFlag := false
			if _, ok := fsm.Get(req.IP); !ok {
				s.logger.Info(fmt.Sprintf("[MsgID: %d, IP: %s] 首次激活机器", req.MsgID, req.IP), nil)

				// 状态机
				fsm.Put(req.IP, &IPState{EndTime: 0, RespChan: make(chan *protofile.LogicResponse, 200), Cancel: cancel})

				chanFlag = true        // 需要激活通知
				hearBeatChan <- req.IP // 激活心跳
			} else {
				s.logger.Info(fmt.Sprintf("[MsgID: %d, IP: %s] 机器正常请求", req.MsgID, req.IP), nil)
			}

			// 业务处理
			resp, err := s.getData(req.IP, ss, req)
			if resp != nil && req.MsgID != 200 {
				s.logger.Info(fmt.Sprintf("[MsgID: %d, IP: %s] 响应的数据: Data: %v, Error: %s", resp.MsgID, resp.IP, resp.Data, resp.Error), nil)
			}

			// 响应错误
			if err != nil {
				s.logger.Error(fmt.Sprintf("[MsgID: %d, IP: %s] 逻辑服务响应错误", req.MsgID, req.IP), err)
				return err
			}

			// 判断是否有响应
			if resp.MsgID != MsgIDNoResponse {
				// 发送响应
				if err = ss.SendMsg(resp); err != nil {
					s.logger.Error(fmt.Sprintf("[MsgID: %d, IP: %s] 逻辑服务发送响应失败", req.MsgID, req.IP), err)
					return errors.Wrap(err, "逻辑服务发送响应失败")
				}
			}

			// 通知启动通知
			if chanFlag {
				noticeChan <- req.IP
				sendNoticeChan <- req.IP
			}

			return nil
		}, 2*time.Microsecond)
	})

	err := eg.Wait()
	return err
}

// checkMsg 验证客户端传递的数据
func checkMsg(req *protofile.LogicRequest, err error) error {
	if err != nil {
		return errors.Wrap(err, "请求接收失败")
	}
	if req == nil {
		return errors.New("请求数据为空")
	}
	if len(req.IP) <= 0 {
		return errors.New("IP地址为空")
	}
	return nil
}

// GetData 业务处理
func (s *Service[SI, NI]) getData(ip string, ss protofile.LogicService_SendServer, req *protofile.LogicRequest) (*protofile.LogicResponse, error) {
	// 业务处理
	f, ok := s.routerMap[req.GetMsgID()]
	if !ok {
		return nil, errors.New("请求业务不存在")
	}

	if f == nil {
		return nil, errors.New("请求业务处理不存在")
	}

	ctx := context.WithValue(ss.Context(), "ip", ip)
	ctx = context.WithValue(ctx,"conn",ss)
	respMsgID, respData, respErr := f(ctx, req.GetData())
	resp := &protofile.LogicResponse{
		MsgID: respMsgID,
		Data:  respData,
	}
	if respErr != nil {
		resp.Error = respErr.Error()
	}

	return resp, nil
}
