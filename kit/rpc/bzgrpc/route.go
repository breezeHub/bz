package bzgrpc

import "context"

// RouterMap 路由
// @return uint32 MsgID
// @return []byte 传输的原始数据
// @return error 错误信息
type RouterMap map[uint32]func(context.Context, []byte) (uint32, []byte, error)
