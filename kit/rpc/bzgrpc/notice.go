package bzgrpc

import (
	"context"
	"fmt"
	"gitee.com/breezeHub/bz/kit/rpc/bzgrpc/protofile"
	"github.com/pkg/errors"
	"sync"
	"time"
)

type NoticeInfo[I any] struct {
	IP      string
	Info    I
	ResChan chan *protofile.LogicResponse

	once   sync.Once
	logger *BizLog
}

// NoticeSend 自定义通知业务内调用的方法
func (n *NoticeInfo[I]) NoticeSend(done bool, f func()) {
	info, ok := fsm.Get(n.IP)
	if done || (ok && info.EndTime != 0 && info.EndTime < time.Now().UnixNano()) {
		n.once.Do(func() {
			n.logger.Info("逻辑服务关闭通知", nil)
			close(n.ResChan)
			fsm.Delete(n.IP)
		})
		return
	}
	f()
}

type NoticeFunc[I any] func(ctx context.Context, msgID uint32, info *NoticeInfo[I])

// Notice 启动通知任务
func (s *Service[SI, NI]) Notice(ctx context.Context, ip string, info NI) {
	wg := sync.WaitGroup{}
	wg.Add(len(s.noticeArray))

	for msgID, noticeFunc := range s.noticeArray {
		msgID := msgID
		noticeFunc := noticeFunc
		go func() {
			defer wg.Done()

			ipState, ok := fsm.Get(ip)
			if ok {
				noticeFunc(ctx, msgID, &NoticeInfo[NI]{
					IP:      ip,
					Info:    info,
					logger:  s.logger,
					ResChan: ipState.RespChan,
				})
			}
		}()
	}

	wg.Wait()
}

// SendNotice 发送通知任务
func (s *Service[SI, NI]) SendNotice(ctx context.Context, ip string, ss protofile.LogicService_SendServer) error {
	for {
		if _, ok := fsm.Get(ip); ok {
			break
		}
		time.Sleep(100 * time.Microsecond)
	}
	info, _ := fsm.Get(ip)

	done := false
	go func() {
		<-ctx.Done()
		done = true
	}()

	// 发送
	for resp := range info.RespChan {
		if done {
			s.logger.Error("逻辑服务发送通知ctx停止", nil)
			return errors.New("逻辑服务发送通知ctx停止")
		}

		s.logger.Info(fmt.Sprintf("逻辑服务发送通知：%#v", resp), nil)
		if err := ss.SendMsg(resp); err != nil {
			s.logger.Error("逻辑服务发送通知失败", err)
			return errors.Wrap(err, "逻辑服务发送通知失败")
		}
	}
	return nil
}
