package main

import (
	"fmt"
	"os"
)

// type Config struct {
// 	Server Server
// }
// type Server struct {
// 	Etcd Etcd
// 	App  App
// }
// type Etcd struct {
// 	Host string
// }
// type App struct {
// 	Name string
// 	Port string
// 	Ip   string
// }

func main() {
	fmt.Printf("os.Args: %v\n", os.Args)

	// bzdebug.D("hello", "world")
	// fmt.Println("-----------")
	// bzdebug.D("hello,%s\n", "breeze")
	// fmt.Println("-----------")

	//var conf Config
	//
	//file := bzyaml.NewFile("./test/config.yml")
	//file.Load(&conf)
	//err := file.LoadAndListen(&conf, func(err error) {
	//	fmt.Println(conf)
	//	log.Println("err", err)
	//})
	//file.LoadAndListenWithTicker(context.Background(), &conf, 2*time.Second, func(err error) {
	//	log.Println("err", err)
	//})
	//
	//tc := bztime.TickerNoExit(context.Background(), func(ctx context.Context) error {
	//	fmt.Println(conf)
	//	return nil
	//}, 2*time.Second)
	//defer tc.Close()
	//
	//for {
	//	select {
	//	case <-tc.C:
	//
	//	}
	//}
	//
	//fmt.Println(conf)
}
