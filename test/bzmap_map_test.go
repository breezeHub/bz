package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/code/bzdump"
	"gitee.com/breezeHub/bz/pkg/data/bzgncmap"
	"testing"
)

func TestBzMap_NewMap(t *testing.T) {
	arr := []string{"a", "b", "c", "d"}

	m := bzgncmap.New[string, int64]()
	for i, s := range arr {
		m.Put(s, int64(i))
	}

	//for _, s := range arr {
	//	val, ok := m.Get(s)
	//	t.Log(val, ok)
	//}
	//val, ok := m.Get("z")
	//t.Log(val, ok)

	m.Range(func(key string, val int64) bool {
		bzdump.Dump(key, val)
		return true
	})
}

type AAA struct {
	name string
}

func TestBzMap_NewMap_GetRand(t *testing.T) {
	arr := []string{"a", "b", "c", "d", "e", "f", "g", "h"}

	m := bzgncmap.New[int64, string]()
	for i, s := range arr {
		m.Put(int64(i), s)
	}
	m1 := m.Search(func(key int64, value string) bool {
		return key > 2
	})
	fmt.Println(m1)

	//m1 := bzmap.NewMap[int64, string]()
	//for i, s := range arr {
	//	m1.Put(int64(i+10), s)
	//}
	//
	//m.Merge(m1)
	//t.Log(m.Result())
	//t.Log(m.Json())

	//b, _ := json.Marshal(map[int64]string{
	//	1: "a",
	//	2: "b",
	//})
	//fmt.Println("b", bzbyte.ToString(b))
	//err := m.AddJson(bzbyte.ToString(b))
	//t.Log(m.Result(), err)

	//st := bzgncmap.New[int64, AAA](bzgncmap.IsUnique[int64, AAA](true))
	//st.Put(1, AAA{name: "aaaa"})
	//st.Put(2, AAA{name: "aaaa"})
	//fmt.Println(st.Result())
	//
	//st.Put(2, AAA{name: "aaaa1"})
	//fmt.Println(st.Result())
	//
	//st.PutMany(map[int64]AAA{
	//	3: {name: "aaaa3"},
	//	5: {name: "aaaa5"},
	//})
	//fmt.Println(st.Result())

	//arr := map[int64]string{0: "a", 1: "b", 2: "c", 3: "d", 4: "e"}
	//targetArr := map[int64]string{0: "a"}
	//ok := bzmap.MatchMapFunc[int64, string](arr, targetArr, func(key int64, value string, targetKey int64, targetValue string) bool {
	//	return value == targetValue
	//})
	//fmt.Println(ok)

	//m := bzgncmap.New[int64, string]()
	//m.PutMany(map[int64]string{
	//	1: "ahaha",
	//	2: "666",
	//	3: "666",
	//})
	//m.FilterUniqueValue()
	//fmt.Println(m.Result())
}
