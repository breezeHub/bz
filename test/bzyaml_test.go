package test

import (
	"gitee.com/breezeHub/bz/pkg/text/bzyaml"
	"log"
	"testing"
)

type Config struct {
	Server Server
}
type Server struct {
	Etcd Etcd
	App  App
}
type Etcd struct {
	Host string
}
type App struct {
	Name string
	Port string
	Ip   string
}

func TestNewFile(t *testing.T) {
	var conf Config

	file := bzyaml.NewFile("./config.yml")
	file.LoadAndListen(&conf, func(err error) {
		log.Println("err", err)
	})
	//file.LoadAndListenWithTicker(context.Background(), &conf, 1*time.Second, func(err error) {
	//	log.Println("err", err)
	//})
	select {}
	t.Log(conf)
}
