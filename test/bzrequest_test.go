package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/net/bzrequest"
	"testing"
	"time"
)

func TestGetHttpResult(t *testing.T) {
	bytes, err := bzrequest.GetHttpResult(
		"https://www.baidu.com/img/flexible/logo/pc/result.png",
		time.Second)
	fmt.Println(bytes, err)
}

func TestDownloadHttpResult(t *testing.T) {
	err := bzrequest.DownloadHttpResult(
		"https://www.baidu.com/img/flexible/logo/pc/result.png",
		"./baidu.png",
		time.Second)
	fmt.Println(err)
}

func TestRequestNew(t *testing.T) {
	req := bzrequest.New(bzrequest.POST, "https://fanyi.baidu.com/langdetect", nil)

	req = req.SetHeader(bzrequest.JsHeader)

	res, err := req.Result(time.Second)
	fmt.Println(res, err)

	resString, err := req.ResultString(time.Second)
	fmt.Println(resString, err)

	type Response struct {
		Errno  int    `json:"errno"`
		Errmsg string `json:"errmsg"`
	}
	// {"errno":1000,"errmsg":"\u672a\u77e5\u9519\u8bef"}
	var r Response
	err = req.ResultAny(time.Second, &r)
	fmt.Println(fmt.Sprintf("%+v", r), err)

	req.Close()
}

func TestRequestGet(t *testing.T) {
	//bytes, err := bzrequest.Get("https://www.baidu.com/img/flexible/logo/pc/result.png", nil,
	//	time.Second, bzrequest.PngHeader)
	//fmt.Println(bytes, err)

	//str, err := bzrequest.GetString("https://fanyi.baidu.com/mtpe/project/getDefaultId", nil,
	//	time.Second, bzrequest.PngHeader)
	//fmt.Println(str, err)

	type Response struct {
		Errno  int    `json:"errno"`
		Errmsg string `json:"errmsg"`
	}
	var r Response
	err := bzrequest.GetAny("https://fanyi.baidu.com/mtpe/project/getDefaultId", nil,
		time.Second, &r, bzrequest.PngHeader)
	fmt.Println(fmt.Sprintf("%+v", r), err)
}
