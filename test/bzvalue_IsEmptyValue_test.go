package test

import (
	"gitee.com/breezeHub/bz/pkg/data/bzvalue"
	"testing"
)

func TestBzValue_IsEmptyValue(t *testing.T) {
	val := ""
	ok := bzvalue.IsEmptyValue(val)
	t.Log(ok)

	val = "123"
	ok = bzvalue.IsEmptyValue(val)
	t.Log(ok)
}
