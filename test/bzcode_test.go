package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/code/bzcode"
	"testing"
)

func TestTryCatch(t *testing.T) {
	bzcode.TryCatch(func() {
		type user struct {
			name string
		}
		var u *user = nil
		fmt.Println(u.name)

	}, func(err bzcode.Exception) {
		fmt.Println("err => ", err.Message())
	})
}

func TestTernary(t *testing.T) {
	// 方式1：常规判断
	b := "hello"
	s := bzcode.Ternary(b == "hello", "yes", "no")
	fmt.Println(s)

	// 方式2：匿名函数判断
	i := bzcode.Ternary(func() bool { return true }(), 1, 2)
	fmt.Println(i)

	// 方式3：嵌套
	n := 8
	res := bzcode.Ternary(n > 5, bzcode.Ternary(n > 7, "大于7", "小于等于7"), "小于等于5")
	// 等同于
	//if n > 5 {
	//	if n > 7 {
	//		res = "大于7"
	//	} else {
	//		res = "小于等于7"
	//	}
	//} else {
	//	res = "小于等于5"
	//}
	fmt.Println(res)
}
