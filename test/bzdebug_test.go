package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/code/bzdump"
	"testing"
)

func TestPrint(t *testing.T) {
	//bzdebug.Dump("hello", "world")

	//bzdebug.DumpF("hello,%s\n", "breeze")

	//fmt.Println("-----------")

	//bzdebug.DumpDie("hello", "world")
	//fmt.Println("-----------")
	//bzdebug.DumpDieF("hello,%s\n", "breeze")
	//fmt.Println("-----------")

	type User struct {
		name string
		age  int
	}
	user := User{
		name: "breeze",
		age:  20,
	}
	bzdump.VarDump(user)
	bzdump.VarDump(2)
	fmt.Println("-----------")
	bzdump.VarDumpDie(user)
	fmt.Println("-----------")
}
