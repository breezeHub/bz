package bzerror

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/code/bzerror"
	"testing"
)

func TestNewErrorGroup(t *testing.T) {
	errorGroup := bzerror.NewErrorGroup[int]()
	errorGroup.AddCode(10000, bzerror.New("test-10000"))
	errorGroup.AddCode(10001, bzerror.New("test-10001"))

	fmt.Println(errorGroup.Get(10001))
	fmt.Println(errorGroup.Get(10002))
}

type (
	BadRequest struct{}
	Error      struct{}
	Success    struct{}
)

func TestNewErrorGroup_struct(t *testing.T) {
	errorGroup := bzerror.NewErrorGroup[any]()
	errorGroup.AddCode(BadRequest{}, bzerror.New("非法请求"))
	errorGroup.AddCode(Error{}, bzerror.New("服务错误"))
	errorGroup.AddCode(Success{}, bzerror.New("成功请求"))

	fmt.Println(errorGroup.Get(Error{}))
	fmt.Println(errorGroup.Get(Success{}))
	fmt.Println(errorGroup.Get(struct{}{}))
}
