package bzwxgzh

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/wechat/bzwxgzh"
	"testing"
)

func TestParseWxRespErr(t *testing.T) {
	resq, err := bzwxgzh.ParseWxRespErr([]byte("{\"errcode\":40013,\"errmsg\":\"invalid appid\"}"))
	fmt.Println(resq, err)
}
