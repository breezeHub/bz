package test

import (
	"fmt"
	"gitee.com/breezeHub/bz/pkg/data/bzmap"
	"testing"
)

func TestContain(t *testing.T) {
	type args struct {
		m     map[int64]string
		value string
	}

	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "包含",
			args: args{
				m:     map[int64]string{0: "a", 1: "b", 2: "c"},
				value: "b",
			},
			want: true,
		},
		{
			name: "未包含",
			args: args{
				m:     map[int64]string{0: "a", 1: "b", 2: "c"},
				value: "d",
			},
			want: false,
		},
		{
			name: "map为nil",
			args: args{
				m:     nil,
				value: "d",
			},
			want: false,
		},
		{
			name: "map为空",
			args: args{
				m:     map[int64]string{},
				value: "d",
			},
			want: false,
		},
		{
			name: "匹配值为空",
			args: args{
				m:     map[int64]string{0: "a", 1: "b", 2: "c"},
				value: "",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := bzmap.Contain(tt.args.m, tt.args.value); got != tt.want {
				t.Errorf("Contain() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContain_run(t *testing.T) {
	m := map[int64]string{0: "a", 1: "b", 2: "c"}
	ok := bzmap.Contain[int64, string](m, "b")
	fmt.Println(ok)
}

func TestMatchMapFunc(t *testing.T) {
	type args struct {
		m1 map[int64]string
		m2 map[int64]string
		fn func(m1Key int64, m1Value string, m2Key int64, m2Value string) bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "包含",
			args: args{
				m1: map[int64]string{0: "a", 1: "b", 2: "c"},
				m2: map[int64]string{0: "c"},
				fn: func(_ int64, m1Value string, _ int64, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: true,
		},
		{
			name: "m2为空",
			args: args{
				m1: map[int64]string{0: "a", 1: "b", 2: "c"},
				m2: map[int64]string{},
				fn: func(_ int64, m1Value string, _ int64, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
		{
			name: "m2为nil",
			args: args{
				m1: map[int64]string{0: "a", 1: "b", 2: "c"},
				m2: nil,
				fn: func(_ int64, m1Value string, _ int64, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
		{
			name: "m1为空",
			args: args{
				m1: map[int64]string{},
				m2: map[int64]string{0: "c"},
				fn: func(_ int64, m1Value string, _ int64, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
		{
			name: "m1为nil",
			args: args{
				m1: nil,
				m2: map[int64]string{0: "c"},
				fn: func(_ int64, m1Value string, _ int64, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := bzmap.MatchMapFunc(tt.args.m1, tt.args.m2, tt.args.fn); got != tt.want {
				t.Errorf("MatchMapFunc() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatchMapFunc_run(t *testing.T) {
	m1 := map[int64]string{0: "a", 1: "b", 2: "c"}
	m2 := map[int64]string{0: "c"}
	ok := bzmap.MatchMapFunc[int64, string](m1, m2, func(_ int64, m1Value string, _ int64, m2Value string) bool {
		return m1Value == m2Value
	})
	fmt.Println(ok)
}

func TestMatchSliceFunc(t *testing.T) {
	type args struct {
		m1 map[int64]string
		m2 []string
		fn func(m1Key int64, m1Value string, m2Value string) bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "包含",
			args: args{
				m1: map[int64]string{0: "a", 1: "b", 2: "c"},
				m2: []string{"c"},
				fn: func(_ int64, m1Value string, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: true,
		},
		{
			name: "m2为空",
			args: args{
				m1: map[int64]string{0: "a", 1: "b", 2: "c"},
				m2: []string{},
				fn: func(_ int64, m1Value string, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
		{
			name: "m2为nil",
			args: args{
				m1: map[int64]string{0: "a", 1: "b", 2: "c"},
				m2: nil,
				fn: func(_ int64, m1Value string, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
		{
			name: "m1为空",
			args: args{
				m1: map[int64]string{},
				m2: []string{"c"},
				fn: func(_ int64, m1Value string, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
		{
			name: "m1为nil",
			args: args{
				m1: nil,
				m2: []string{"c"},
				fn: func(_ int64, m1Value string, m2Value string) bool {
					return m1Value == m2Value
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := bzmap.MatchSliceFunc(tt.args.m1, tt.args.m2, tt.args.fn); got != tt.want {
				t.Errorf("MatchSliceFunc() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatchSliceFunc_run(t *testing.T) {
	m1 := map[int64]string{0: "a", 1: "b", 2: "c"}
	m2 := []string{"c"}
	ok := bzmap.MatchSliceFunc[int64, string](m1, m2, func(_ int64, m1Value string, m2Value string) bool {
		return m1Value == m2Value
	})
	fmt.Println(ok)
}
