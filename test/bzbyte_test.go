package test

import (
	"gitee.com/breezeHub/bz/pkg/code/bzdump"
	"gitee.com/breezeHub/bz/pkg/data/bzbyte"
	"gitee.com/breezeHub/bz/pkg/data/bzstring"
	"testing"
)

var s = "adsfasdfadsfadsfasdfadfadfasdfasdfadsfasdfasdfasdfsadfas"

// b2s converts byte slice to a string without memory allocation.
// See https://groups.google.com/forum/#!msg/Golang-Nuts/ENgbUzYvCuU/90yGx7GUAgAJ .
//
// Note it may break if string and/or slice header will change
// in the future go versions.
func b2s(b []byte) string {
	/* #nosec G103 */
	//return *(*string)(unsafe.Pointer(&b))

	return bzbyte.ToString(b)
}

// s2b converts string to a byte slice without memory allocation.
//
// Note it may break if string and/or slice header will change
// in the future go versions.
func s2b(s string) (b []byte) {
	/* #nosec G103 */
	//bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	/* #nosec G103 */
	//sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	//bh.Data = sh.Data
	//bh.Cap = sh.Len
	//bh.Len = sh.Len
	//return b

	return bzstring.ToByte(s)
}

func BenchmarkB2sFast(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = s2b(s)
	}
}

func BenchmarkB2sStd(b *testing.B) {
	var _ []byte
	for i := 0; i < b.N; i++ {
		_ = []byte(s)
	}
}

var bt = []byte("adsfasdfadsfadsfasdfadfadfasdfasdfadsfasdfasdfasdfsadfas")

func BenchmarkS2BFast(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = b2s(bt)
	}
}

func BenchmarkS2BStd(b *testing.B) {
	var _ []byte
	for i := 0; i < b.N; i++ {
		_ = string(bt)
	}
}

//goos: windows
//goarch: amd64
//pkg: gitee.com/breezeHub/bz/test
//cpu: 12th Gen Intel(R) Core(TM) i7-12700
//BenchmarkB2sFast
//BenchmarkB2sFast-20     1000000000               0.1121 ns/op
//BenchmarkB2sStd
//BenchmarkB2sStd-20      66312561                18.07 ns/op
//BenchmarkS2BFast
//BenchmarkS2BFast-20     1000000000               0.1107 ns/op
//BenchmarkS2BStd
//BenchmarkS2BStd-20      73933964                15.96 ns/op
//PASS

//func TestToString(t *testing.T) {
//	s := "test"
//	b := bzstring.ToByte(s)
//	t.Log(b)
//}
//
//func TestToByte(t *testing.T) {
//	b := []byte{116, 101, 115, 116}
//	s := bzbyte.ToString(b)
//	t.Log(s)
//}

func TestNewBuffer(t *testing.T) {
	b := bzbyte.NewBuffer()
	_ = b.Put("你", "好")
	bzdump.DD(b.String())
}
